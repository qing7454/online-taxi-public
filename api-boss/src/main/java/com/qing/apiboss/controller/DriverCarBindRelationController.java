package com.qing.apiboss.controller;

import com.qing.apiboss.service.DriverCarBindRelationService;
import com.qing.internalcommon.dto.DriverCarBindingRelationship;
import com.qing.internalcommon.dto.ResponseResult;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/driver-car-binding-relationship")
@RequiredArgsConstructor
public class DriverCarBindRelationController {

    private final DriverCarBindRelationService driverCarBindRelationService;

    @PostMapping(path = "/bind")
    public ResponseResult<?> bind(@RequestBody DriverCarBindingRelationship relationship) {
        return driverCarBindRelationService.bind(relationship);
    }

    @PostMapping(path = "/unbind")
    public ResponseResult<?> unbind(@RequestBody DriverCarBindingRelationship relationship) {
        return driverCarBindRelationService.unbind(relationship);
    }
}

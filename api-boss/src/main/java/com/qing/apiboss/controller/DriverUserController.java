package com.qing.apiboss.controller;

import com.qing.apiboss.service.CarService;
import com.qing.apiboss.service.DriverUserService;
import com.qing.internalcommon.dto.Car;
import com.qing.internalcommon.dto.DriverUser;
import com.qing.internalcommon.dto.ResponseResult;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@Slf4j
public class DriverUserController {

    private final DriverUserService driverUserService;

    private final CarService carService;

    @PostMapping(path = "/driver-user")
    public ResponseResult<?> addDriverUser(@RequestBody DriverUser driverUser) {
        return driverUserService.addDriverUser(driverUser);
    }

    @PutMapping(path = "/driver-user")
    public ResponseResult<?> updateDriverUser(@RequestBody DriverUser driverUser) {
        return driverUserService.updateDriverUser(driverUser);
    }

    @PostMapping(path = "/car")
    public ResponseResult<?> addCar(@RequestBody Car car) {
        return carService.addCar(car);
    }
}

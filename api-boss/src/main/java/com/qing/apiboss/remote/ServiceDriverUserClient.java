package com.qing.apiboss.remote;

import com.qing.internalcommon.dto.Car;
import com.qing.internalcommon.dto.DriverCarBindingRelationship;
import com.qing.internalcommon.dto.DriverUser;
import com.qing.internalcommon.dto.ResponseResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient("service-driver-user")
public interface ServiceDriverUserClient {

    @PostMapping(path = "/user")
    ResponseResult<?> addDriverUser(@RequestBody DriverUser driverUser);

    @PutMapping(path = "/user")
    ResponseResult<?> updateDriverUser(@RequestBody DriverUser driverUser);

    @PostMapping(path = "/car")
    ResponseResult<?> addCar(@RequestBody Car car);

    @PostMapping(path = "/driver-car-binding-relationship/bind")
    ResponseResult<?> bind(@RequestBody DriverCarBindingRelationship relationship);

    @PostMapping(path = "/driver-car-binding-relationship/unbind")
    ResponseResult<?> unbind(@RequestBody DriverCarBindingRelationship relationship);

}

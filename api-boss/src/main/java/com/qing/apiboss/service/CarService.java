package com.qing.apiboss.service;

import com.qing.apiboss.remote.ServiceDriverUserClient;
import com.qing.internalcommon.dto.Car;
import com.qing.internalcommon.dto.ResponseResult;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CarService {

    private final ServiceDriverUserClient serviceDriverUserClient;

    public ResponseResult<?> addCar(Car car) {
        return serviceDriverUserClient.addCar(car);
    }
}

package com.qing.apiboss.service;

import com.qing.apiboss.remote.ServiceDriverUserClient;
import com.qing.internalcommon.dto.DriverCarBindingRelationship;
import com.qing.internalcommon.dto.ResponseResult;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class DriverCarBindRelationService {

    private final ServiceDriverUserClient serviceDriverUserClient;

    public ResponseResult<?> bind(DriverCarBindingRelationship relationship) {
        return serviceDriverUserClient.bind(relationship);
    }

    public ResponseResult<?> unbind(DriverCarBindingRelationship relationship) {
        return serviceDriverUserClient.unbind(relationship);
    }
}

package com.qing.apiboss.service;

import com.qing.apiboss.remote.ServiceDriverUserClient;
import com.qing.internalcommon.dto.DriverUser;
import com.qing.internalcommon.dto.ResponseResult;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class DriverUserService {

    private final ServiceDriverUserClient serviceDriverUserClient;

    public ResponseResult<?> addDriverUser(DriverUser driverUser) {
        return serviceDriverUserClient.addDriverUser(driverUser);
    }

    public ResponseResult<?> updateDriverUser(DriverUser driverUser) {
        return serviceDriverUserClient.updateDriverUser(driverUser);
    }
}

package com.qing.apidriver.controller;

import com.qing.apidriver.service.DriverOrderService;
import com.qing.internalcommon.dto.ResponseResult;
import com.qing.internalcommon.request.OrderRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/driver-order")
public class DriverOrderController {

    private final DriverOrderService driverOrderService;

    /**
     * 去接乘客
     * @param orderRequest
     * @return
     */
    @PostMapping(path = "/to-pickup-passenger")
    public ResponseResult<?> toPickupPassenger(@RequestBody OrderRequest orderRequest) {
        return driverOrderService.toPickupPassenger(orderRequest);
    }

    /**
     * 抵达乘客上车点
     * @param orderRequest
     * @return
     */
    @PostMapping(path = "/arrived-departure")
    public ResponseResult<?> arrivedDeparture(@RequestBody OrderRequest orderRequest) {
        return driverOrderService.arrivedDeparture(orderRequest);
    }

    /**
     * 接到乘客
     * @param orderRequest
     * @return
     */
    @PostMapping(path = "/pickup-passenger")
    public ResponseResult<?> pickupPassenger(@RequestBody OrderRequest orderRequest) {
        return driverOrderService.pickupPassenger(orderRequest);
    }

    /**
     * 乘客到达目的地下车
     * @param orderRequest
     * @return
     */
    @PostMapping(path = "/passenger-get-off")
    public ResponseResult<?> passengerGetOff(@RequestBody OrderRequest orderRequest) {
        return driverOrderService.passengerGetOff(orderRequest);
    }

    /**
     * 取消订单
     * @param orderId 订单ID
     * @return
     */
    @PostMapping(path = "/cancel")
    public ResponseResult<?> cancel(@RequestParam String orderId) {
        return driverOrderService.cancel(orderId);
    }
}

package com.qing.apidriver.controller;

import com.qing.apidriver.service.UserService;
import com.qing.internalcommon.dto.DriverUser;
import com.qing.internalcommon.dto.ResponseResult;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class DriverUserController {

    private final UserService userService;

    @PutMapping(path = "/user")
    public ResponseResult<?> updateDriverUser(@RequestBody DriverUser driverUser) {
        return userService.updateUser(driverUser);
    }
}

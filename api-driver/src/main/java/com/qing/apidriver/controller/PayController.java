package com.qing.apidriver.controller;

import com.qing.apidriver.service.PayService;
import com.qing.internalcommon.dto.ResponseResult;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/pay")
public class PayController {

    private final PayService payService;

    /**
     * 发起付款
     * @param orderId
     * @param passengerId
     * @param price
     * @return
     */
    @PostMapping(path = "/push-pay-info")
    public ResponseResult<?> pushPayInfo(@RequestParam String orderId, @RequestParam String passengerId, @RequestParam String price) {
        return payService.pushPayInfo(orderId, passengerId, price);
    }
}

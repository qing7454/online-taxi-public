package com.qing.apidriver.controller;

import com.qing.apidriver.service.PointService;
import com.qing.internalcommon.dto.ResponseResult;
import com.qing.internalcommon.request.ApiDriverPointRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/point")
public class PointController {

    private final PointService pointService;

    @PostMapping(path = "/upload")
    public ResponseResult<?> upload(@RequestBody ApiDriverPointRequest request) {
        return pointService.upload(request);
    }
}

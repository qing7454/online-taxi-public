package com.qing.apidriver.controller;

import com.qing.apidriver.service.VerificationCodeService;
import com.qing.internalcommon.constant.CommonStatusEnum;
import com.qing.internalcommon.dto.ResponseResult;
import com.qing.internalcommon.request.VerificationCodeDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@RequiredArgsConstructor
public class VerificationCodeController {

    private final VerificationCodeService verificationCodeService;

    @GetMapping(path = "/verification-code")
    public ResponseResult<?> verificationCode(@RequestBody VerificationCodeDTO verificationCodeDTO) {
        String driverPhone = verificationCodeDTO.getDriverPhone();
        log.info("driverPhone: {}", driverPhone);
        if (null == driverPhone) {
            return ResponseResult.fail(CommonStatusEnum.PHONE_NOT_EMPTY);
        }
        return verificationCodeService.verificationCode(driverPhone);
    }

    @PostMapping(path = "/verification-code-check")
    public ResponseResult<?> verificationCodeCheck(@RequestBody VerificationCodeDTO verificationCodeDTO) {
        String driverPhone = verificationCodeDTO.getDriverPhone();
        String verificationCode = verificationCodeDTO.getVerificationCode();
        if (StringUtils.isBlank(driverPhone)) {
            return ResponseResult.fail(CommonStatusEnum.PHONE_NOT_EMPTY);
        }
        if (StringUtils.isBlank(verificationCode)) {
            return ResponseResult.fail(CommonStatusEnum.VERIFICATION_CODE_ERROR);
        }
        return verificationCodeService.verificationCodeCheck(driverPhone, verificationCode);
    }
}

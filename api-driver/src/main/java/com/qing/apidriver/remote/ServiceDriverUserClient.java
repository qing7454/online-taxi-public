package com.qing.apidriver.remote;

import com.qing.internalcommon.dto.Car;
import com.qing.internalcommon.dto.DriverUser;
import com.qing.internalcommon.dto.ResponseResult;
import com.qing.internalcommon.response.DriverUserExistsResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

@FeignClient("service-driver-user")
public interface ServiceDriverUserClient {

    @PutMapping(path = "/user")
    ResponseResult<?> updateDriverUser(@RequestBody DriverUser driverUser);

    @GetMapping(path = "/check-driver/{driverPhone}")
    ResponseResult<DriverUserExistsResponse> checkDriver(@PathVariable("driverPhone") String driverPhone);

    @GetMapping(path = "/car")
    ResponseResult<Car> getCarById(@RequestParam("carId") Long carId);
}

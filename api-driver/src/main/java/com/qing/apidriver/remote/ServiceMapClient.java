package com.qing.apidriver.remote;

import com.qing.internalcommon.dto.ResponseResult;
import com.qing.internalcommon.request.PointRequest;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient("service-map")
public interface ServiceMapClient {

    @PostMapping(path = "/point/upload")
    ResponseResult<?> upload(@RequestBody PointRequest pointRequest);
}

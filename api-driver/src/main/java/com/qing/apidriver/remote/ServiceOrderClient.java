package com.qing.apidriver.remote;

import com.qing.internalcommon.dto.ResponseResult;
import com.qing.internalcommon.request.OrderRequest;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient("service-order")
public interface ServiceOrderClient {

    @PostMapping(path = "/order/to-pickup-passenger")
    ResponseResult<?> toPickupPassenger(@RequestBody OrderRequest orderRequest);

    @PostMapping(path = "/order/arrived-departure")
    ResponseResult<?> arrivedDeparture(@RequestBody OrderRequest orderRequest);

    @PostMapping(path = "/order/pickup-passenger")
    ResponseResult<?> pickupPassenger(@RequestBody OrderRequest orderRequest);

    @PostMapping(path = "/order/passenger-get-off")
    ResponseResult<?> passengerGetOff(@RequestBody OrderRequest orderRequest);

    @PostMapping(path = "/order/cancel")
    ResponseResult<?> cancel(@RequestParam String orderId, @RequestParam String identity);
}

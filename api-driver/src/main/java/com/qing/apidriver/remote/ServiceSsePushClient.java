package com.qing.apidriver.remote;

import com.qing.internalcommon.dto.ResponseResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient("service-sse-push")
public interface ServiceSsePushClient {

    @GetMapping(path = "/push")
    ResponseResult<String> push(@RequestParam Long userId, @RequestParam String msg, @RequestParam("identity") String identity);
}

package com.qing.apidriver.service;

import com.qing.apidriver.remote.ServiceOrderClient;
import com.qing.internalcommon.constant.IdentityConstant;
import com.qing.internalcommon.dto.ResponseResult;
import com.qing.internalcommon.request.OrderRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class DriverOrderService {

    private final ServiceOrderClient serviceOrderClient;

    /**
     * 司机去接乘客
     * @param orderRequest
     * @return
     */
    public ResponseResult<?> toPickupPassenger(OrderRequest orderRequest) {
        return serviceOrderClient.toPickupPassenger(orderRequest);
    }

    /**
     * 到达乘客下单地点
     * @param orderRequest
     * @return
     */
    public ResponseResult<?> arrivedDeparture(OrderRequest orderRequest) {
        return serviceOrderClient.arrivedDeparture(orderRequest);
    }

    /**
     * 接到乘客
     * @param orderRequest
     * @return
     */
    public ResponseResult<?> pickupPassenger(OrderRequest orderRequest) {
        return serviceOrderClient.pickupPassenger(orderRequest);
    }

    /**
     * 乘客到达目的地下车，结束行程
     * @param orderRequest
     * @return
     */
    public ResponseResult<?> passengerGetOff(OrderRequest orderRequest) {
        return serviceOrderClient.passengerGetOff(orderRequest);
    }

    /**
     * 取消订单
     * @param orderId 订单ID
     * @return
     */
    public ResponseResult<?> cancel(String orderId) {
        return serviceOrderClient.cancel(orderId, IdentityConstant.DRIVER_IDENTITY);
    }
}

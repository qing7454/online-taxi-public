package com.qing.apidriver.service;

import com.alibaba.fastjson2.JSONObject;
import com.qing.apidriver.remote.ServiceSsePushClient;
import com.qing.internalcommon.constant.IdentityConstant;
import com.qing.internalcommon.dto.ResponseResult;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PayService {

    private final ServiceSsePushClient serviceSsePushClient;

    public ResponseResult<?> pushPayInfo(String orderId, String passengerId, String price) {
        JSONObject msg = new JSONObject();
        msg.put("orderId", orderId);
        msg.put("price", price);
        Long userId = Long.valueOf(passengerId);
        serviceSsePushClient.push(userId, msg.toString(), IdentityConstant.PASSENGER_IDENTITY);
        return ResponseResult.success();
    }
}

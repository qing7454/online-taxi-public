package com.qing.apidriver.service;

import com.qing.apidriver.remote.ServiceDriverUserClient;
import com.qing.apidriver.remote.ServiceMapClient;
import com.qing.internalcommon.dto.Car;
import com.qing.internalcommon.dto.ResponseResult;
import com.qing.internalcommon.request.ApiDriverPointRequest;
import com.qing.internalcommon.request.PointRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PointService {

    private final ServiceDriverUserClient serviceDriverUserClient;

    private final ServiceMapClient serviceMapClient;

    public ResponseResult<?> upload(ApiDriverPointRequest apiDriverPointRequest) {
        // hu获取carId
        Long carId = apiDriverPointRequest.getCarId();
        ResponseResult<Car> carResponse = serviceDriverUserClient.getCarById(carId);
        Car car = carResponse.getData();
        // 通过carId，调用service-driver-user获取tid，trid
        String tid = car.getTid();
        String trid = car.getTrid();

        // 调用地图服务service-map上传轨迹点
        PointRequest pointRequest = new PointRequest();
        pointRequest.setTid(tid);
        pointRequest.setTrid(trid);
        pointRequest.setPoints(apiDriverPointRequest.getPoints());
        return serviceMapClient.upload(pointRequest);
    }
}

package com.qing.apidriver.service;

import com.qing.apidriver.remote.ServiceDriverUserClient;
import com.qing.internalcommon.dto.DriverUser;
import com.qing.internalcommon.dto.ResponseResult;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
public class UserService {

    private final ServiceDriverUserClient serviceDriverUserClient;

    public ResponseResult<?> updateUser(DriverUser driverUser) {
        driverUser.setGmtModified(LocalDateTime.now());
        return serviceDriverUserClient.updateDriverUser(driverUser);
    }
}

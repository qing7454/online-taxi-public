package com.qing.apidriver.service;

import com.qing.apidriver.remote.ServiceDriverUserClient;
import com.qing.apidriver.remote.ServiceVerificationCodeClient;
import com.qing.internalcommon.constant.CommonStatusEnum;
import com.qing.internalcommon.constant.DriverCarConstants;
import com.qing.internalcommon.constant.IdentityConstant;
import com.qing.internalcommon.constant.TokenConstant;
import com.qing.internalcommon.dto.ResponseResult;
import com.qing.internalcommon.response.DriverUserExistsResponse;
import com.qing.internalcommon.response.NumberCodeResponse;
import com.qing.internalcommon.response.TokenResponse;
import com.qing.internalcommon.util.JwtUtils;
import com.qing.internalcommon.util.RedisPrefixUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

@Service
@RequiredArgsConstructor
@Slf4j
public class VerificationCodeService {

    /**
     * 验证码过期时间
     */
    private static final int VERIFY_CODE_TIMEOUT = 2;

    private final ServiceVerificationCodeClient serviceVerificationCodeClient;

    private final ServiceDriverUserClient serviceDriverUserClient;

    private final StringRedisTemplate stringRedisTemplate;

    public ResponseResult<?> verificationCode(String driverPhone) {
        if (StringUtils.isBlank(driverPhone)) {
            return ResponseResult.fail(CommonStatusEnum.PHONE_NOT_EMPTY);
        }
        // 检查司机用户是否存在
        ResponseResult<DriverUserExistsResponse> responseResult = serviceDriverUserClient.checkDriver(driverPhone);
        int code = responseResult.getCode();
        if (CommonStatusEnum.SUCCESS.getCode() == code) {
            DriverUserExistsResponse driverUserExists = responseResult.getData();
            Integer exists = driverUserExists.getExists();
            if (DriverCarConstants.DRIVER_NOT_EXISTS == exists) {
                //不存在
                log.error("手机号为【{}】用户不存在", driverPhone);
                return ResponseResult.fail(CommonStatusEnum.DRIVER_NOT_EXISTS);
            }
        }

        // 获取验证码
        ResponseResult<NumberCodeResponse> numberCodeResult = serviceVerificationCodeClient.verificationCode(6);
        NumberCodeResponse data = numberCodeResult.getData();
        Integer numberCode = data.getNumberCode();
        log.info("验证码：{}", numberCode);

        // 调用第三方短信接口
        log.info("调用第三方短信接口");


        // 存到redis
        log.info("将验证码存到redis");
        String key = RedisPrefixUtil.getKeyByPhone(driverPhone, IdentityConstant.DRIVER_IDENTITY);
        stringRedisTemplate.opsForValue().set(key, numberCode+"", VERIFY_CODE_TIMEOUT, TimeUnit.MINUTES);

        return ResponseResult.success(data);
    }

    /**
     * 校验验证码
     * @param driverPhone 手机号码
     * @param verificationCode 验证码
     * @return result
     */
    public ResponseResult<?> verificationCodeCheck(String driverPhone, String verificationCode) {
        log.info("verify info: {}", driverPhone);
        // 根据手机号，获取redis验证码
        String key = RedisPrefixUtil.getKeyByPhone(driverPhone, IdentityConstant.DRIVER_IDENTITY);
        String keyRedis = stringRedisTemplate.opsForValue().get(key);
        log.info("redis key: {}", keyRedis);
        // 校验验证码
        if (StringUtils.isBlank(keyRedis) || !Objects.equals(keyRedis.trim(), verificationCode.trim())) {
            return ResponseResult.fail(CommonStatusEnum.VERIFICATION_CODE_ERROR.getCode(), CommonStatusEnum.VERIFICATION_CODE_ERROR.getValue());
        }

        // 颁发token
        String accessToken = JwtUtils.generateToken(driverPhone, IdentityConstant.DRIVER_IDENTITY, TokenConstant.ACCESS_TOKEN_TYPE);
        String refreshToken = JwtUtils.generateToken(driverPhone, IdentityConstant.DRIVER_IDENTITY, TokenConstant.REFRESH_TOKEN_TYPE);
        // 存储token到redis
        String accessTokenKey = RedisPrefixUtil.generateTokenKey(driverPhone, IdentityConstant.DRIVER_IDENTITY, TokenConstant.ACCESS_TOKEN_TYPE);
        stringRedisTemplate.opsForValue().set(accessTokenKey, accessToken, TokenConstant.PASSENGER_ACCESS_TOKEN_TIMEOUT, TimeUnit.DAYS);
        String refreshTokenKey = RedisPrefixUtil.generateTokenKey(driverPhone, IdentityConstant.DRIVER_IDENTITY, TokenConstant.REFRESH_TOKEN_TYPE);
        stringRedisTemplate.opsForValue().set(refreshTokenKey, refreshToken, TokenConstant.PASSENGER_REFRESH_TOKEN_TIMEOUT, TimeUnit.DAYS);

        TokenResponse tokenResponse = new TokenResponse();
        tokenResponse.setAccessToken(accessToken);
        tokenResponse.setRefreshToken(refreshToken);
        return ResponseResult.success(tokenResponse);
    }
}

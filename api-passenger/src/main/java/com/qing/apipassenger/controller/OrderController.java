package com.qing.apipassenger.controller;

import com.qing.apipassenger.service.OrderService;
import com.qing.internalcommon.request.OrderRequest;
import com.qing.internalcommon.dto.ResponseResult;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
@RequestMapping(path = "/order")
@RequiredArgsConstructor
public class OrderController {

    private final OrderService orderService;

    /**
     * 创建订单，乘客下订单
     * @param orderRequest 订单信息
     * @return result
     */
    @PostMapping(path = "/add")
    public ResponseResult<?> add(@RequestBody OrderRequest orderRequest) {
        log.info("order info: {}", orderRequest);
        return orderService.create(orderRequest);
    }

    @PostMapping(path = "/cancel")
    public ResponseResult<?> cancel(@RequestParam String orderId) {
        return orderService.cancel(orderId);
    }
}

package com.qing.apipassenger.controller;

import com.qing.apipassenger.remote.ServiceOrderClient;
import com.qing.internalcommon.dto.OrderInfo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@RequiredArgsConstructor
public class TestController {

    private final ServiceOrderClient serviceOrderClient;

    @GetMapping(path = "/test")
    public String test() {
        return "api passenger";
    }

    @GetMapping(path = "/authTest")
    public String authTest() {
        return "auth test";
    }

    @GetMapping(path = "/noauthTest")
    public String noauthTest() {
        return "noauth test";
    }

    @GetMapping(path = "/test-real-time-order/{orderId}")
    public String testRealTimeOrder(@PathVariable("orderId") Long orderId) {
        log.info("测试并发订单orderId：{}", orderId);
        serviceOrderClient.testRealTimeOrder(orderId);
        return "test-real-time-order success.";
    }
}

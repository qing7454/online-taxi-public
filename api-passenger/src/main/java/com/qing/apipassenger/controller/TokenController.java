package com.qing.apipassenger.controller;

import com.qing.apipassenger.service.TokenService;
import com.qing.internalcommon.dto.ResponseResult;
import com.qing.internalcommon.response.TokenResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@Slf4j
public class TokenController {

    private final TokenService tokenService;

    @PostMapping(path = "/token-refresh")
    public ResponseResult<?> refreshToken(@RequestBody TokenResponse tokenResponse) {
        String refreshTokenSrc = tokenResponse.getRefreshToken();
        log.info("原来的refreshToken: {}", refreshTokenSrc);
        return tokenService.refreshToken(refreshTokenSrc);
    }
}

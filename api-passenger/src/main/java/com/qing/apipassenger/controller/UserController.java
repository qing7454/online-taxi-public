package com.qing.apipassenger.controller;

import com.qing.apipassenger.service.UserService;
import com.qing.internalcommon.dto.ResponseResult;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @GetMapping(path = "/users")
    public ResponseResult<?> getUser(HttpServletRequest request) {
        String authorization = request.getHeader("Authorization");
        return userService.getUserByToken(authorization);
    }
}

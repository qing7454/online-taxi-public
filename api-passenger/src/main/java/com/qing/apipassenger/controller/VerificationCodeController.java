package com.qing.apipassenger.controller;

import com.qing.apipassenger.request.VerificationCodeDTO;
import com.qing.apipassenger.service.VerificationCodeService;
import com.qing.internalcommon.dto.ResponseResult;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@RequiredArgsConstructor
public class VerificationCodeController {

    private final VerificationCodeService verificationCodeService;

    @GetMapping(path = "/verification-code")
    public ResponseResult<?> getCode(@RequestBody @Validated VerificationCodeDTO verificationCodeDTO) {
        log.info("receive phone number: {}", verificationCodeDTO);
        return verificationCodeService.generateCode(verificationCodeDTO.getPassengerPhone());
    }

    @PostMapping(path = "/verification-code-check")
    public ResponseResult<?> checkCode(@RequestBody VerificationCodeDTO verificationCodeDTO) {
        return verificationCodeService.checkCode(verificationCodeDTO);
    }
}

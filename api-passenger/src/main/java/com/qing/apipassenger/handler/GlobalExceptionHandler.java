package com.qing.apipassenger.handler;

import com.qing.internalcommon.constant.CommonStatusEnum;
import com.qing.internalcommon.dto.ResponseResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseResult<?> validationExceptionHandler(MethodArgumentNotValidException exception) {
        return ResponseResult.fail(CommonStatusEnum.VALIDATE_ERROR.getCode(), exception.getBindingResult().getAllErrors().get(0).getDefaultMessage());
    }
}

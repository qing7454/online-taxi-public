package com.qing.apipassenger.interceptor;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.auth0.jwt.exceptions.AlgorithmMismatchException;
import com.auth0.jwt.exceptions.SignatureVerificationException;
import com.auth0.jwt.exceptions.TokenExpiredException;
import com.qing.internalcommon.constant.TokenConstant;
import com.qing.internalcommon.dto.ResponseResult;
import com.qing.internalcommon.dto.TokenResult;
import com.qing.internalcommon.util.JwtUtils;
import com.qing.internalcommon.util.RedisPrefixUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.util.Objects;

@Component
public class JwtInterceptor implements HandlerInterceptor {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        boolean result = false;
        String msg = "";
        TokenResult tokenResult = null;

        String authorization = request.getHeader("Authorization");
        if (StringUtils.isBlank(authorization)) {
            msg = "please input token";
        }
        try {
            tokenResult = JwtUtils.parseToken(authorization);
            result = true;
        } catch (SignatureVerificationException e) {
            msg = "signature token fail";
        } catch (TokenExpiredException e) {
            msg = "token expired";
        } catch (AlgorithmMismatchException e) {
            msg = "token algorithm error";
        } catch (Exception e) {
            msg = "token invalid";
        }

        if (null == tokenResult) {
            msg = "token invalid";
        } else {
            String identity = tokenResult.getIdentity();
            String phone = tokenResult.getPhone();
            String tokenKey = RedisPrefixUtil.generateTokenKey(phone, identity, TokenConstant.ACCESS_TOKEN_TYPE);
            String token = stringRedisTemplate.opsForValue().get(tokenKey);
            if (StringUtils.isBlank(token)) {
                msg = "token invalid";
                result = false;
            } else {
                if (!Objects.equals(token.trim(), authorization.trim())) {
                    msg = "access token invalid";
                    result = false;
                }
            }
        }

        if (!result) {
            PrintWriter writer = response.getWriter();
            Object json = JSON.toJSONString(ResponseResult.fail(msg));
            writer.println(json);
        }

        return result;
    }
}

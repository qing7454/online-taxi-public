package com.qing.apipassenger.remote;

import com.qing.internalcommon.dto.ResponseResult;
import com.qing.internalcommon.request.OrderRequest;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

@FeignClient("service-order")
public interface ServiceOrderClient {

    @PostMapping(path = "/order/add")
    ResponseResult<?> add(@RequestBody OrderRequest orderRequest);

    @GetMapping(path = "/test-real-time-order/{orderId}")
    String testRealTimeOrder(@PathVariable("orderId") Long orderId);

    @PostMapping(path = "/order/cancel")
    ResponseResult<?> cancel(@RequestParam String orderId, @RequestParam String identity);
}

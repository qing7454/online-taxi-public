package com.qing.apipassenger.remote;

import com.qing.apipassenger.request.VerificationCodeDTO;
import com.qing.internalcommon.dto.PassengerUser;
import com.qing.internalcommon.dto.ResponseResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient("service-passenger-user")
public interface ServicePassengerUserClient {

    @PostMapping(path = "/user")
    ResponseResult<?> signInOrSignUp(@RequestBody VerificationCodeDTO verificationCodeDTO);

    @GetMapping(path = "/user/{phone}")
    ResponseResult<PassengerUser> getUser(@PathVariable("phone") String phone);
}

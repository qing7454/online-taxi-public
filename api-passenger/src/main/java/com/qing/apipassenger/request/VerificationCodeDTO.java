package com.qing.apipassenger.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Data
public class VerificationCodeDTO {

    /**
     * 乘客手机号
     */
    @NotBlank(message = "手机号不能为空")
    @Pattern(regexp = "^(1[3-9]\\d{9})$", message = "手机号格式不正确")
    private String passengerPhone;

    /**
     * 验证码
     */
    private String verificationCode;

    /**
     * 司机手机号
     */
    private String driverPhone;
}

package com.qing.apipassenger.response;

import lombok.Data;

@Data
public class Result {

    private int code;
    private String message;

    public Result() {}

    public Result(Integer code, String msg) {
        this.code = code;
        this.message = msg;
    }

    public static Result ok(Integer code, String msg) {
        return new Result(code, msg);
    }

}

package com.qing.apipassenger.service;

import com.qing.apipassenger.remote.ServicePriceClient;
import com.qing.internalcommon.dto.ForecastPriceDTO;
import com.qing.internalcommon.dto.ResponseResult;
import com.qing.internalcommon.response.ForecastPriceResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
@Slf4j
@RequiredArgsConstructor
public class ForecastPriceService {

    private final ServicePriceClient servicePriceClient;

    /**
     * 价格预算服务
     * @param depLongitude 出发地经度
     * @param depLatitude 出发地纬度
     * @param destLongitude 目的地经度
     * @param destLatitude 目的地纬度
     * @return 价格
     */
    public ResponseResult<?> forecastPrice(String depLongitude, String depLatitude,
                                           String destLongitude, String destLatitude,
                                           String cityCode, String vehicleType) {
        log.info("出发地经度-depLongitude: {}", depLongitude);
        log.info("出发地纬度-depLatitude: {}", depLatitude);
        log.info("目的地经度-destLongitude: {}", destLongitude);
        log.info("目的地纬度-destLatitude: {}", destLatitude);
        log.info("城市编码-cityCode: {}", cityCode);
        log.info("车辆型号-vehicleType: {}", vehicleType);

        log.info("调用计价服务，计算价格");
        ForecastPriceDTO forecastPriceDTO = new ForecastPriceDTO();
        forecastPriceDTO.setDepLongitude(depLongitude);
        forecastPriceDTO.setDepLatitude(depLatitude);
        forecastPriceDTO.setDestLongitude(destLongitude);
        forecastPriceDTO.setDestLatitude(destLatitude);
        forecastPriceDTO.setCityCode(cityCode);
        forecastPriceDTO.setVehicleType(vehicleType);
        ResponseResult<ForecastPriceResponse> forecastPriceResponseResponseResult = servicePriceClient.forecastPrice(forecastPriceDTO);
        ForecastPriceResponse data = forecastPriceResponseResponseResult.getData();
        BigDecimal price = data.getPrice();
        String fareType = data.getFareType();
        Integer fareVersion = data.getFareVersion();

        ForecastPriceResponse forecastPriceResponse = new ForecastPriceResponse();
        forecastPriceResponse.setPrice(price);
        forecastPriceResponse.setCityCode(cityCode);
        forecastPriceResponse.setVehicleType(vehicleType);
        forecastPriceResponse.setFareType(fareType);
        forecastPriceResponse.setFareVersion(fareVersion);
        return ResponseResult.success(forecastPriceResponse);
    }
}

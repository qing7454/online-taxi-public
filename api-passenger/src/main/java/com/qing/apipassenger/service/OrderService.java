package com.qing.apipassenger.service;

import com.qing.apipassenger.remote.ServiceOrderClient;
import com.qing.internalcommon.constant.IdentityConstant;
import com.qing.internalcommon.dto.ResponseResult;
import com.qing.internalcommon.request.OrderRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class OrderService {

    private final ServiceOrderClient serviceOrderClient;

    public ResponseResult<?> create(OrderRequest orderRequest) {
        return serviceOrderClient.add(orderRequest);
    }

    public ResponseResult<?> cancel(String orderId) {
        return serviceOrderClient.cancel(orderId, IdentityConstant.PASSENGER_IDENTITY);
    }
}

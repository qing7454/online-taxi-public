package com.qing.apipassenger.service;

import com.qing.internalcommon.constant.CommonStatusEnum;
import com.qing.internalcommon.constant.IdentityConstant;
import com.qing.internalcommon.constant.TokenConstant;
import com.qing.internalcommon.dto.ResponseResult;
import com.qing.internalcommon.dto.TokenResult;
import com.qing.internalcommon.response.TokenResponse;
import com.qing.internalcommon.util.JwtUtils;
import com.qing.internalcommon.util.RedisPrefixUtil;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

@Service
@RequiredArgsConstructor
public class TokenService {

    private final StringRedisTemplate stringRedisTemplate;

    public ResponseResult<?> refreshToken(String refreshTokenSrc) {
        // 解析refreshToken
        TokenResult tokenResult = JwtUtils.checkToken(refreshTokenSrc);
        if (tokenResult == null) {
            return ResponseResult.fail(CommonStatusEnum.TOKEN_ERROR.getCode(), CommonStatusEnum.TOKEN_ERROR.getValue());
        }

        String identity = tokenResult.getIdentity();
        String phone = tokenResult.getPhone();
        String refreshTokenKey = RedisPrefixUtil.generateTokenKey(phone, identity, TokenConstant.REFRESH_TOKEN_TYPE);
        String refreshToken = stringRedisTemplate.opsForValue().get(refreshTokenKey);
        if (StringUtils.isBlank(refreshToken) || !Objects.equals(refreshTokenSrc.trim(), refreshToken.trim())) {
            return ResponseResult.fail(CommonStatusEnum.TOKEN_ERROR.getCode(), CommonStatusEnum.TOKEN_ERROR.getValue());
        }
        // 生成token
        String accessTokenNew = JwtUtils.generateToken(phone, IdentityConstant.PASSENGER_IDENTITY, TokenConstant.ACCESS_TOKEN_TYPE);
        String refreshTokenNew = JwtUtils.generateToken(phone, IdentityConstant.PASSENGER_IDENTITY, TokenConstant.REFRESH_TOKEN_TYPE);
        // 存储token到redis
        String accessTokenKeyNew = RedisPrefixUtil.generateTokenKey(phone, IdentityConstant.PASSENGER_IDENTITY, TokenConstant.ACCESS_TOKEN_TYPE);
        stringRedisTemplate.opsForValue().set(accessTokenKeyNew, accessTokenNew, TokenConstant.PASSENGER_ACCESS_TOKEN_TIMEOUT, TimeUnit.DAYS);
        String refreshTokenKeyNew = RedisPrefixUtil.generateTokenKey(phone, IdentityConstant.PASSENGER_IDENTITY, TokenConstant.REFRESH_TOKEN_TYPE);
        stringRedisTemplate.opsForValue().set(refreshTokenKeyNew, refreshTokenNew, TokenConstant.PASSENGER_REFRESH_TOKEN_TIMEOUT, TimeUnit.DAYS);

        TokenResponse tokenResponse = new TokenResponse();
        tokenResponse.setRefreshToken(refreshTokenNew);
        tokenResponse.setAccessToken(accessTokenNew);

        return ResponseResult.success(tokenResponse);
    }
}

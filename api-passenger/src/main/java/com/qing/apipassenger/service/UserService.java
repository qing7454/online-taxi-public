package com.qing.apipassenger.service;

import com.qing.apipassenger.remote.ServicePassengerUserClient;
import com.qing.internalcommon.dto.PassengerUser;
import com.qing.internalcommon.dto.ResponseResult;
import com.qing.internalcommon.dto.TokenResult;
import com.qing.internalcommon.util.JwtUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class UserService {

    private final ServicePassengerUserClient servicePassengerUserClient;

    public ResponseResult<?> getUserByToken(String token) {
        log.info("user access token: {}", token);
        TokenResult tokenResult = JwtUtils.checkToken(token);
        String phone = tokenResult.getPhone();
        log.info("user phone: {}", phone);

        ResponseResult<PassengerUser> responseResult = servicePassengerUserClient.getUser(phone);
        PassengerUser passengerUser = responseResult.getData();
        return ResponseResult.success(passengerUser);
    }
}

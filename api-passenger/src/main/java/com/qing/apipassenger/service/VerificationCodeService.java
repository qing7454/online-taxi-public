package com.qing.apipassenger.service;

import com.qing.apipassenger.remote.ServicePassengerUserClient;
import com.qing.apipassenger.remote.ServiceVerificationCodeClient;
import com.qing.apipassenger.request.VerificationCodeDTO;
import com.qing.internalcommon.constant.CommonStatusEnum;
import com.qing.internalcommon.constant.IdentityConstant;
import com.qing.internalcommon.constant.TokenConstant;
import com.qing.internalcommon.dto.ResponseResult;
import com.qing.internalcommon.response.NumberCodeResponse;
import com.qing.internalcommon.response.TokenResponse;
import com.qing.internalcommon.util.JwtUtils;
import com.qing.internalcommon.util.RedisPrefixUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Service
@RequiredArgsConstructor
@Slf4j
public class VerificationCodeService {

    private final ServiceVerificationCodeClient serviceVerificationCodeClient;

    private final StringRedisTemplate stringRedisTemplate;

    private final ServicePassengerUserClient servicePassengerUserClient;

    /**
     * 验证码长度
     */
    private static final Integer VERIFY_CODE_LENGTH = 6;

    /**
     * 验证码有效时长，单位：分钟
     */
    private static final int PASSENGER_VERIFICATION_CODE_TIMEOUT = 2;

    public ResponseResult<?> generateCode(String passengerPhone) {
        log.info("调用验证码服务，获取验证码");
        String code = "112233";

        ResponseResult<NumberCodeResponse> responseResult = serviceVerificationCodeClient.getNumberCode(VERIFY_CODE_LENGTH);
        Integer numberCode = responseResult.getData().getNumberCode();
        log.info("remote service return code: {}", numberCode);

        //存入redis
        log.info("存入redis");
        String numberCodeStr = numberCode + "";
        String key = RedisPrefixUtil.getKeyByPhone(passengerPhone, IdentityConstant.PASSENGER_IDENTITY);
        stringRedisTemplate.opsForValue().set(key, numberCodeStr, PASSENGER_VERIFICATION_CODE_TIMEOUT, TimeUnit.MINUTES);

        return ResponseResult.success("");
    }

    public ResponseResult<?> checkCode(VerificationCodeDTO verificationCodeDTO) {
        log.info("verify info: {}", verificationCodeDTO);
        // 根据手机号，获取redis验证码
        String key = RedisPrefixUtil.getKeyByPhone(verificationCodeDTO.getPassengerPhone(), IdentityConstant.PASSENGER_IDENTITY);
        String keyRedis = stringRedisTemplate.opsForValue().get(key);
        log.info("redis key: {}", keyRedis);
        // 校验验证码
        if (StringUtils.isBlank(keyRedis) || !Objects.equals(keyRedis.trim(), verificationCodeDTO.getVerificationCode().trim())) {
            return ResponseResult.fail(CommonStatusEnum.VERIFICATION_CODE_ERROR.getCode(), CommonStatusEnum.VERIFICATION_CODE_ERROR.getValue());
        }

        // 判断是否存在该手机号用户，有就查询，没有就插入记录
        ResponseResult<?> responseResult = servicePassengerUserClient.signInOrSignUp(verificationCodeDTO);
        log.info("user service result: {}", responseResult);
        // 颁发token
        String accessToken = JwtUtils.generateToken(verificationCodeDTO.getPassengerPhone(), IdentityConstant.PASSENGER_IDENTITY, TokenConstant.ACCESS_TOKEN_TYPE);
        String refreshToken = JwtUtils.generateToken(verificationCodeDTO.getPassengerPhone(), IdentityConstant.PASSENGER_IDENTITY, TokenConstant.REFRESH_TOKEN_TYPE);
        // 存储token到redis
        String accessTokenKey = RedisPrefixUtil.generateTokenKey(verificationCodeDTO.getPassengerPhone(), IdentityConstant.PASSENGER_IDENTITY, TokenConstant.ACCESS_TOKEN_TYPE);
        stringRedisTemplate.opsForValue().set(accessTokenKey, accessToken, TokenConstant.PASSENGER_ACCESS_TOKEN_TIMEOUT, TimeUnit.DAYS);
        String refreshTokenKey = RedisPrefixUtil.generateTokenKey(verificationCodeDTO.getPassengerPhone(), IdentityConstant.PASSENGER_IDENTITY, TokenConstant.REFRESH_TOKEN_TYPE);
        stringRedisTemplate.opsForValue().set(refreshTokenKey, refreshToken, TokenConstant.PASSENGER_REFRESH_TOKEN_TIMEOUT, TimeUnit.DAYS);

        TokenResponse tokenResponse = new TokenResponse();
        tokenResponse.setAccessToken(accessToken);
        tokenResponse.setRefreshToken(refreshToken);
        return ResponseResult.success(tokenResponse);
    }

}

package com.qing.internalcommon.constant;

public class AmapConfigConstants {

    /**
     * 路径规划
     */
    public static final String DIRECTION_URL = "https://restapi.amap.com/v3/direction/driving";

    /**
     * 行政区域查询
     */
    public static final String DISTRICT_URL = "https://restapi.amap.com/v3/config/district";

    /**
     * 添加服务
     */
    public static final String SERVICE_ADD_URL = "https://tsapi.amap.com/v1/track/service/add?key=%s&name=%s";

    /**
     * 添加终端
     */
    public static final String TERMINAL_ADD_URL = "https://tsapi.amap.com/v1/track/terminal/add?key=%s&sid=%s&name=%s&desc=%s";

    /**
     * 添加轨迹
     */
    public static final String TRACT_ADD_URL = "https://tsapi.amap.com/v1/track/trace/add?key=%s&sid=%s&tid=%s";

    /**
     * 轨迹点上传
     */
    public static final String POINT_UPLOAD_URL = "https://tsapi.amap.com/v1/track/point/upload";

    /**
     * 周边终端搜索
     */
    public static final String TERMINAL_AROUND_SEARCH_URL = "https://tsapi.amap.com/v1/track/terminal/aroundsearch?key=%s&sid=%s&center=%s&radius=%s";

    /**
     * 终端轨迹查询
     */
    public static final String TERMINAL_TRSEARCH_URL = "https://tsapi.amap.com/v1/track/terminal/trsearch?key=%s&sid=%s&tid=%s&starttime=%d&endtime=%d";

    public static final String STATUS_KEY = "status";

    public static final String ROUTE_KEY = "route";
    public static final String PATHS_KEY = "paths";
    public static final String DISTANCE_KEY = "distance";
    public static final String DURATION_KEY = "duration";

    /**
     * 调用服务创建：成功
     */
    public static final int SERVICE_SUCCESS = 10000;

    /**
     * 调用服务创建：失败
     */
    public static final int SERVICE_ERROR = 20001;

}

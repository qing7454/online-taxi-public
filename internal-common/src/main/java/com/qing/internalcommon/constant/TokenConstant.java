package com.qing.internalcommon.constant;

public class TokenConstant {

    /**
     * 访问token
     */
    public static final String ACCESS_TOKEN_TYPE = "accessToken";

    /**
     * 刷新token
     */
    public static final String REFRESH_TOKEN_TYPE = "refreshToken";

    /**
     * access token过期时间，单位：天
     */
    public static final int PASSENGER_ACCESS_TOKEN_TIMEOUT = 30;

    /**
     * refresh token 过期时间 单位：天
     */
    public static final int PASSENGER_REFRESH_TOKEN_TIMEOUT = 31;
}

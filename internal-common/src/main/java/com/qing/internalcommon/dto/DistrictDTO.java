package com.qing.internalcommon.dto;

import lombok.Data;

import java.util.List;

@Data
public class DistrictDTO {

    private String status;
    private String info;
    private String infocode;
    private String count;
    private List<DistrictItemDTO> districts;

    @Data
    public static class DistrictItemDTO {
        private String citycode;
        private String adcode;
        private String name;
        private String center;
        private String level;
        private List<DistrictItemDTO> districts;
    }
}

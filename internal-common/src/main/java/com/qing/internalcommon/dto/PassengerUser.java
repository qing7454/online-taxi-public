package com.qing.internalcommon.dto;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class PassengerUser {

    private Long id;
    private String passengerName;
    private String passengerPhone;
    private String profilePhoto;
    private Byte passengerGender;
    private Byte state;
    private LocalDateTime createTime;
    private LocalDateTime updateTime;

}

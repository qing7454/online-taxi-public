package com.qing.internalcommon.dto;

import com.qing.internalcommon.constant.CommonStatusEnum;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class ResponseResult<T> {

    private int code;
    private String message;
    private T data;

    /**
     * 默认成功响应的方法
     * @return result
     */
    public static ResponseResult<String> success() {
        return new ResponseResult<String>()
                .setCode(CommonStatusEnum.SUCCESS.getCode())
                .setData("")
                .setMessage(CommonStatusEnum.SUCCESS.getValue());
    }

    /**
     * 成功响应的方法
     * @param data 数据
     * @return result
     * @param <T> 数据类型
     */
    public static <T> ResponseResult<T> success(T data) {
        return new ResponseResult<T>()
                .setCode(CommonStatusEnum.SUCCESS.getCode())
                .setData(data)
                .setMessage(CommonStatusEnum.SUCCESS.getValue());
    }

    /**
     * 自定义返回失败结果
     * @param msg 错误信息
     * @return result
     */
    public static ResponseResult<String> fail(String msg) {
        return new ResponseResult<String>().setCode(CommonStatusEnum.FAIL.getCode()).setMessage(msg).setData("");
    }

    /**
     * 自定义返回失败结果
     * @param statusEnum 错误枚举类
     * @return result
     */
    public static ResponseResult<String> fail(CommonStatusEnum statusEnum) {
        return new ResponseResult<String>().setCode(statusEnum.getCode()).setMessage(statusEnum.getValue());
    }

    /**
     * 自定义返回失败结果
     * @param code 失败状态码
     * @param msg 失败信息
     * @return result
     */
    public static ResponseResult<String> fail(int code, String msg) {
        return new ResponseResult<String>().setCode(code).setMessage(msg);
    }

    /**
     * 自定义返回失败结果
     * @param code 失败状态码
     * @param msg 失败信息
     * @param data 数据
     * @return result
     */
    public static ResponseResult<String> fail(int code, String msg, String data) {
        return new ResponseResult<String>().setCode(code).setMessage(msg).setData(data);
    }
}

package com.qing.internalcommon.request;

import lombok.Data;

@Data
public class CalculatePriceRequest {

    private Long distance;
    private Long duration;
    private String cityCode;
    private String vehicleType;
}

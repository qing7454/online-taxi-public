package com.qing.internalcommon.request;

import lombok.Data;

@Data
public class PointRequest {

    private String key;
    private String sid;
    private String tid;
    private String trid;
    private PointDTO[] points;
}

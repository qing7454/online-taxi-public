package com.qing.internalcommon.response;

import lombok.Data;

@Data
public class AmapServiceResponse {

    private Integer errcode;
    private Object errdetail;
    private String errmsg;
    private AmapServiceData data;

    @Data
    public static class AmapServiceData {
        private String name;
        private Integer sid;
        private Integer tid;
        private String desc;
        private Long createtime;
        private Long locatetime;
        private Object props;
    }
}

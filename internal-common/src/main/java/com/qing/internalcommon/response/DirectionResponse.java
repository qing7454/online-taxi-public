package com.qing.internalcommon.response;

import lombok.Data;

@Data
public class DirectionResponse {

    private Long distance;
    private Long duration;
}

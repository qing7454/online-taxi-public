package com.qing.internalcommon.response;

import lombok.Data;

@Data
public class NumberCodeResponse {

    private Integer numberCode;

}

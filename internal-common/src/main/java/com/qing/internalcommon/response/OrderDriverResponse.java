package com.qing.internalcommon.response;

import lombok.Data;

@Data
public class OrderDriverResponse {

    private Long driverId;
    private String driverPhone;
    private Long carId;

    private String vehicleNo;
    private String licenseId;
}

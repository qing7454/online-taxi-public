package com.qing.internalcommon.response;

import lombok.Data;

@Data
public class ServiceResponse {

    private Integer sid;
    private Integer tid;
}

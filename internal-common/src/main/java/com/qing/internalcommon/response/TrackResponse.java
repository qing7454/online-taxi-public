package com.qing.internalcommon.response;

import lombok.Data;

@Data
public class TrackResponse {

    private String trid;
    private String trname;
}

package com.qing.internalcommon.response.alipay;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * Auto-generated: 2022-12-20 15:43:53
 *
 * @author fanrenqing
 */
@Data
public class AlipayCallbackResult {

    private String gmt_create;
    private String charset;
    private String gmt_payment;
    private String notify_time;
    private String subject ;
    private Long buyer_id;
    private BigDecimal invoice_amount;
    private Integer version;
    private String notify_id;
    private List<FundBillList> fund_bill_list ;
    private String notify_type;
    private Long out_trade_no;
    private BigDecimal total_amount;
    private String trade_status;
    private Long trade_no;
    private Long auth_app_id;
    private Integer receipt_amount;
    private Integer point_amount;
    private Long app_id;
    private Integer buyer_pay_amount;
    private Long seller_id;

}
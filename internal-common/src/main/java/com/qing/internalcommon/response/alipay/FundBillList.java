package com.qing.internalcommon.response.alipay;

import lombok.Data;

/**
 * Auto-generated: 2022-12-20 15:43:53
 *
 * @author fanrenqing
 */
@Data
public class FundBillList {

    private String amount;
    private String fundChannel;

}
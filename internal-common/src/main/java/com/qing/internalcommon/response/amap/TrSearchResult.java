package com.qing.internalcommon.response.amap;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "data",
    "errcode",
    "errdetail",
    "errmsg"
})
public class TrSearchResult {

    @JsonProperty("data")
    private TrSearchData data;
    @JsonProperty("errcode")
    private Integer errcode;
    @JsonProperty("errdetail")
    private Object errdetail;
    @JsonProperty("errmsg")
    private String errmsg;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("data")
    public TrSearchData getData() {
        return data;
    }

    @JsonProperty("data")
    public void setData(TrSearchData data) {
        this.data = data;
    }

    @JsonProperty("errcode")
    public Integer getErrcode() {
        return errcode;
    }

    @JsonProperty("errcode")
    public void setErrcode(Integer errcode) {
        this.errcode = errcode;
    }

    @JsonProperty("errdetail")
    public Object getErrdetail() {
        return errdetail;
    }

    @JsonProperty("errdetail")
    public void setErrdetail(Object errdetail) {
        this.errdetail = errdetail;
    }

    @JsonProperty("errmsg")
    public String getErrmsg() {
        return errmsg;
    }

    @JsonProperty("errmsg")
    public void setErrmsg(String errmsg) {
        this.errmsg = errmsg;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(TrSearchData.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("data");
        sb.append('=');
        sb.append(((this.data == null)?"<null>":this.data));
        sb.append(',');
        sb.append("errcode");
        sb.append('=');
        sb.append(((this.errcode == null)?"<null>":this.errcode));
        sb.append(',');
        sb.append("errdetail");
        sb.append('=');
        sb.append(((this.errdetail == null)?"<null>":this.errdetail));
        sb.append(',');
        sb.append("errmsg");
        sb.append('=');
        sb.append(((this.errmsg == null)?"<null>":this.errmsg));
        sb.append(',');
        sb.append("additionalProperties");
        sb.append('=');
        sb.append(((this.additionalProperties == null)?"<null>":this.additionalProperties));
        sb.append(',');
        if (sb.charAt((sb.length()- 1)) == ',') {
            sb.setCharAt((sb.length()- 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = ((result* 31)+((this.errcode == null)? 0 :this.errcode.hashCode()));
        result = ((result* 31)+((this.errdetail == null)? 0 :this.errdetail.hashCode()));
        result = ((result* 31)+((this.errmsg == null)? 0 :this.errmsg.hashCode()));
        result = ((result* 31)+((this.additionalProperties == null)? 0 :this.additionalProperties.hashCode()));
        result = ((result* 31)+((this.data == null)? 0 :this.data.hashCode()));
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof TrSearchResult) == false) {
            return false;
        }
        TrSearchResult rhs = ((TrSearchResult) other);
        return ((((((this.errcode == rhs.errcode)||((this.errcode!= null)&&this.errcode.equals(rhs.errcode)))&&((this.errdetail == rhs.errdetail)||((this.errdetail!= null)&&this.errdetail.equals(rhs.errdetail))))&&((this.errmsg == rhs.errmsg)||((this.errmsg!= null)&&this.errmsg.equals(rhs.errmsg))))&&((this.additionalProperties == rhs.additionalProperties)||((this.additionalProperties!= null)&&this.additionalProperties.equals(rhs.additionalProperties))))&&((this.data == rhs.data)||((this.data!= null)&&this.data.equals(rhs.data))));
    }

}

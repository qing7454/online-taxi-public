package com.qing.internalcommon.response.amap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "counts",
    "degradedParams",
    "distance",
    "endPoint",
    "points",
    "startPoint",
    "time",
    "trid"
})
public class Track {

    @JsonProperty("counts")
    private Integer counts;
    @JsonProperty("degradedParams")
    private DegradedParams degradedParams;
    @JsonProperty("distance")
    private Long distance;
    @JsonProperty("endPoint")
    private EndPoint endPoint;
    @JsonProperty("points")
    private List<Point> points = new ArrayList<Point>();
    @JsonProperty("startPoint")
    private StartPoint startPoint;
    @JsonProperty("time")
    private Long time;
    @JsonProperty("trid")
    private Integer trid;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("counts")
    public Integer getCounts() {
        return counts;
    }

    @JsonProperty("counts")
    public void setCounts(Integer counts) {
        this.counts = counts;
    }

    @JsonProperty("degradedParams")
    public DegradedParams getDegradedParams() {
        return degradedParams;
    }

    @JsonProperty("degradedParams")
    public void setDegradedParams(DegradedParams degradedParams) {
        this.degradedParams = degradedParams;
    }

    @JsonProperty("distance")
    public Long getDistance() {
        return distance;
    }

    @JsonProperty("distance")
    public void setDistance(Long distance) {
        this.distance = distance;
    }

    @JsonProperty("endPoint")
    public EndPoint getEndPoint() {
        return endPoint;
    }

    @JsonProperty("endPoint")
    public void setEndPoint(EndPoint endPoint) {
        this.endPoint = endPoint;
    }

    @JsonProperty("points")
    public List<Point> getPoints() {
        return points;
    }

    @JsonProperty("points")
    public void setPoints(List<Point> points) {
        this.points = points;
    }

    @JsonProperty("startPoint")
    public StartPoint getStartPoint() {
        return startPoint;
    }

    @JsonProperty("startPoint")
    public void setStartPoint(StartPoint startPoint) {
        this.startPoint = startPoint;
    }

    @JsonProperty("time")
    public Long getTime() {
        return time;
    }

    @JsonProperty("time")
    public void setTime(Long time) {
        this.time = time;
    }

    @JsonProperty("trid")
    public Integer getTrid() {
        return trid;
    }

    @JsonProperty("trid")
    public void setTrid(Integer trid) {
        this.trid = trid;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(Track.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("counts");
        sb.append('=');
        sb.append(((this.counts == null)?"<null>":this.counts));
        sb.append(',');
        sb.append("degradedParams");
        sb.append('=');
        sb.append(((this.degradedParams == null)?"<null>":this.degradedParams));
        sb.append(',');
        sb.append("distance");
        sb.append('=');
        sb.append(((this.distance == null)?"<null>":this.distance));
        sb.append(',');
        sb.append("endPoint");
        sb.append('=');
        sb.append(((this.endPoint == null)?"<null>":this.endPoint));
        sb.append(',');
        sb.append("points");
        sb.append('=');
        sb.append(((this.points == null)?"<null>":this.points));
        sb.append(',');
        sb.append("startPoint");
        sb.append('=');
        sb.append(((this.startPoint == null)?"<null>":this.startPoint));
        sb.append(',');
        sb.append("time");
        sb.append('=');
        sb.append(((this.time == null)?"<null>":this.time));
        sb.append(',');
        sb.append("trid");
        sb.append('=');
        sb.append(((this.trid == null)?"<null>":this.trid));
        sb.append(',');
        sb.append("additionalProperties");
        sb.append('=');
        sb.append(((this.additionalProperties == null)?"<null>":this.additionalProperties));
        sb.append(',');
        if (sb.charAt((sb.length()- 1)) == ',') {
            sb.setCharAt((sb.length()- 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = ((result* 31)+((this.endPoint == null)? 0 :this.endPoint.hashCode()));
        result = ((result* 31)+((this.distance == null)? 0 :this.distance.hashCode()));
        result = ((result* 31)+((this.counts == null)? 0 :this.counts.hashCode()));
        result = ((result* 31)+((this.startPoint == null)? 0 :this.startPoint.hashCode()));
        result = ((result* 31)+((this.time == null)? 0 :this.time.hashCode()));
        result = ((result* 31)+((this.additionalProperties == null)? 0 :this.additionalProperties.hashCode()));
        result = ((result* 31)+((this.degradedParams == null)? 0 :this.degradedParams.hashCode()));
        result = ((result* 31)+((this.points == null)? 0 :this.points.hashCode()));
        result = ((result* 31)+((this.trid == null)? 0 :this.trid.hashCode()));
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Track) == false) {
            return false;
        }
        Track rhs = ((Track) other);
        return ((((((((((this.endPoint == rhs.endPoint)||((this.endPoint!= null)&&this.endPoint.equals(rhs.endPoint)))&&((this.distance == rhs.distance)||((this.distance!= null)&&this.distance.equals(rhs.distance))))&&((this.counts == rhs.counts)||((this.counts!= null)&&this.counts.equals(rhs.counts))))&&((this.startPoint == rhs.startPoint)||((this.startPoint!= null)&&this.startPoint.equals(rhs.startPoint))))&&((this.time == rhs.time)||((this.time!= null)&&this.time.equals(rhs.time))))&&((this.additionalProperties == rhs.additionalProperties)||((this.additionalProperties!= null)&&this.additionalProperties.equals(rhs.additionalProperties))))&&((this.degradedParams == rhs.degradedParams)||((this.degradedParams!= null)&&this.degradedParams.equals(rhs.degradedParams))))&&((this.points == rhs.points)||((this.points!= null)&&this.points.equals(rhs.points))))&&((this.trid == rhs.trid)||((this.trid!= null)&&this.trid.equals(rhs.trid))));
    }

}
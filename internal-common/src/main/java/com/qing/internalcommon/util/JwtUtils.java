package com.qing.internalcommon.util;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.qing.internalcommon.constant.TokenConstant;
import com.qing.internalcommon.dto.TokenResult;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class JwtUtils {

    /**
     * 盐
     */
    public static final String SIGN = "CPFqing!@#$$";

    /**
     * jwt key
     */
    public static final String JWT_KEY_PHONE = "phone";

    /**
     * 用户身份信息 1：乘客 2：司机
     */
    public static final String JWT_KEY_IDENTITY = "identity";

    /**
     * token类型
     */
    public static final String JWT_KEY_TOKEN_TYPE = "tokenType";

    /**
     * 生成token
     * @return token
     */
    public static String generateToken(String passengerPhone, String identity, String tokenType) {
        Map<String, String> map = new HashMap<>();
        map.put(JWT_KEY_PHONE, passengerPhone);
        map.put(JWT_KEY_IDENTITY, identity);
        map.put(JWT_KEY_TOKEN_TYPE, tokenType);
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, 1);
        Date date = calendar.getTime();

        JWTCreator.Builder builder = JWT.create();
        map.forEach(builder::withClaim);
//        builder.withExpiresAt(date);
        return builder.sign(Algorithm.HMAC256(SIGN));
    }

    /**
     * 解析token
     * @param token jwt token
     * @return source token
     */
    public static TokenResult parseToken(String token) {
        DecodedJWT verify = JWT.require(Algorithm.HMAC256(SIGN)).build().verify(token);
        String phone = verify.getClaim(JWT_KEY_PHONE).asString();
        String identity = verify.getClaim(JWT_KEY_IDENTITY).asString();
        TokenResult tokenResult = new TokenResult();
        tokenResult.setIdentity(identity);
        tokenResult.setPhone(phone);

        return tokenResult;
    }

    /**
     * 校验token，主要判断token是否异常
     * @param token
     * @return
     */
    public static TokenResult checkToken(String token){
        TokenResult tokenResult = null;
        try {
            tokenResult = JwtUtils.parseToken(token);
        }catch (Exception e){

        }
        return tokenResult;
    }


    public static void main(String[] args) {
        String token = generateToken("13415327477", "1", TokenConstant.ACCESS_TOKEN_TYPE);
        System.out.println(token);
        TokenResult tokenResult = parseToken(token);
        System.out.println("--------------解析------------------");
        System.out.println("电话号码：" + tokenResult.getPhone());
        System.out.println("身份信息：" + tokenResult.getIdentity());
    }
}

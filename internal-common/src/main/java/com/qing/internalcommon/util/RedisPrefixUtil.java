package com.qing.internalcommon.util;

public class RedisPrefixUtil {

    /**
     * 乘客验证码key
     */
    private static final String VERIFICATION_CODE_PREFIX = "verification-code-%s-%s";

    /**
     * token prefix
     */
    private static final String TOKEN_KEY_PREFIX = "token-%s-%s-%s";

    // 黑名单设备号
    public static String blackDeviceCodePrefix = "black-device-%s";

    /**
     * 根据手机号获取redis中的key值
     * @param phone 电话号码
     * @param identity 用户身份类型：1：乘客 2：司机
     * @return key
     */
    public static String getKeyByPhone(String phone, String identity) {
        return String.format(VERIFICATION_CODE_PREFIX, identity, phone);
    }

    /**
     * 获取token的redis key
     * @param passengerPhone 乘客电话号码
     * @return key
     */
    public static String generateTokenKey(String passengerPhone, String identity, String keyType) {
        return String.format(TOKEN_KEY_PREFIX, passengerPhone, identity, keyType);
    }
}

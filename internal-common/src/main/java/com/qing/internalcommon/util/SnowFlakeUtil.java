package com.qing.internalcommon.util;
 
import java.io.Serializable;
import java.lang.management.ManagementFactory;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;
 
//import org.hibernate.HibernateException;
//import org.hibernate.engine.spi.SharedSessionContractImplementor;
//import org.hibernate.id.IdentityGenerator;
 
/**
 * 雪花算法
 * 
 * @author yh
 *
 */
public class SnowFlakeUtil /*extends IdentityGenerator*/ {
	private final static long TWEPOCH = 1288834974657L;
	private final static long WORKER_ID_BITS = 5L;
	private final static long DATACENTER_ID_BITS = 5L;
	private final static long SEQUENCE_BITS = 12L;
	private final static long WORKER_ID_SHIFT = SEQUENCE_BITS;
	private final static long DATACENTER_ID_SHIFT = SEQUENCE_BITS + WORKER_ID_BITS;
	private final static long TIMESTAMP_LEFT_SHIFT = SEQUENCE_BITS + WORKER_ID_BITS + DATACENTER_ID_BITS;
	private final static long SEQUENCE_MASK = -1L ^ (-1L << SEQUENCE_BITS);
	private static long workerMask = -1L ^ (-1L << WORKER_ID_BITS);
	private static long processMask = -1L ^ (-1L << DATACENTER_ID_BITS);
	private static long lastTimeStamp = -1L;
	private long sequence = 0L;
	private long workerId = getMachineNum() & workerMask;
	private long processId = (Long.valueOf(ManagementFactory.getRuntimeMXBean().getName().split("@")[0])) & processMask;
 
	private synchronized long getId() {
		long timestamp = timeStamp();
		if (timestamp < lastTimeStamp) {
			try {
				throw new Exception("Clock moved backwards.  Refusing to generate id for " + (lastTimeStamp - timestamp)
						+ " milliseconds");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		if (lastTimeStamp == timestamp) {
			sequence = ++sequence & SEQUENCE_MASK;
			if (sequence == 0) {
				timestamp = tilNextMillis(lastTimeStamp);
			}
		} else {
			sequence = 0;
		}
		lastTimeStamp = timestamp;
		return ((timestamp - TWEPOCH) << TIMESTAMP_LEFT_SHIFT) | (processId << DATACENTER_ID_SHIFT)
				| (workerId << WORKER_ID_SHIFT) | sequence;
	}
 
	private long tilNextMillis(final long lastTimestamp) {
		long timestamp = timeStamp();
		while (timestamp <= lastTimestamp) {
			timestamp = timeStamp();
		}
		return timestamp;
	}
 
	private long timeStamp() {
		return System.currentTimeMillis();
	}
 
	private static long getMachineNum() {
		StringBuilder sb = new StringBuilder();
		Enumeration<NetworkInterface> enumeration = null;
		try {
			enumeration = NetworkInterface.getNetworkInterfaces();
		} catch (SocketException e) {
			e.printStackTrace();
		}
		while (enumeration.hasMoreElements()) {
			NetworkInterface ni = enumeration.nextElement();
			sb.append(ni.toString());
		}
		return sb.toString().hashCode();
	}
 
//	@Override
//	public Serializable generate(SharedSessionContractImplementor session, Object object) throws HibernateException {
//		return getId();
//	}
	
}

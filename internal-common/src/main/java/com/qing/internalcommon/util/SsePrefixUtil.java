package com.qing.internalcommon.util;

public class SsePrefixUtil {

    private static final String separator = "$";

    public static String generateSseKey(Long userId, String identity) {
        return String.format("%s%s%s", userId, separator, identity);
    }
}

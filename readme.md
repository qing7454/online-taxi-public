## 接口管理
| 服务名                      | 端口号  |
|--------------------------|------|
| api-passenger            | 8081 |
| service-passenger-user   | 8082 |
| service-verificationcode | 8083 |
| service-price            | 8084 |
| service-map              | 8085 |
| service-order            | 8088 |
| service-driver-user      | 8086 |
| service-boss             | 8087 |
| service-alipay           | 8089 |
| sse-driver-client-web    | 9000 |

package com.qing.servicealipay.config;

import com.alipay.easysdk.factory.Factory;
import com.alipay.easysdk.kernel.Config;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

@Configuration
@ConfigurationProperties(prefix = "alipay")
@Data
public class AlipayConfig {

    private String appId;
    private String appPrivateKey;
    private String publicKey;
    private String notifyUrl;

    @PostConstruct
    public void init() {
        Config config = new Config();
        config.protocol = "https";
        config.gatewayHost = "openapi.alipaydev.com";
        config.signType = "RSA2";

        config.appId = this.appId;
        config.alipayPublicKey = this.publicKey;
        config.merchantPrivateKey = this.appPrivateKey;
        config.notifyUrl = this.notifyUrl;
        Factory.setOptions(config);
        System.out.println("支付宝配置初始化完成");
    }
}

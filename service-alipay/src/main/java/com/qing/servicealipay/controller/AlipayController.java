package com.qing.servicealipay.controller;

import cn.hutool.core.bean.BeanUtil;
import com.alipay.easysdk.factory.Factory;
import com.alipay.easysdk.kernel.util.ResponseChecker;
import com.alipay.easysdk.payment.page.models.AlipayTradePagePayResponse;
import com.qing.internalcommon.dto.ResponseResult;
import com.qing.internalcommon.response.alipay.AlipayCallbackResult;
import com.qing.servicealipay.service.AlipayService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@Controller
@Slf4j
@RequiredArgsConstructor
@RequestMapping(path = "/alipay")
public class AlipayController {

    private final AlipayService alipayService;

    @GetMapping(path = "/pay")
    @ResponseBody
    public String doPay(String subject, String outTradeNo, String totalAmount) {
        AlipayTradePagePayResponse response;
        try {
            // 2. 发起API调用（以创建当面付收款二维码为例）
            response = Factory.Payment.Page().pay(subject, outTradeNo, totalAmount, "");
            // 3. 处理响应或异常
            if (ResponseChecker.success(response)) {
                log.info("调用成功: {}", response.getBody());
            } else {
                log.error("调用失败，原因：{}", response.getBody());
            }
        } catch (Exception e) {
            log.error("调用遭遇异常，原因：{}", e.getMessage());
            throw new RuntimeException(e.getMessage(), e);
        }
        return response.getBody();
    }

    @PostMapping(path = "/callback")
    @ResponseBody
    public String payCallback(HttpServletRequest request) throws Exception {
        log.info("支付宝支付成功回调");
        String tradeStatus = request.getParameter("trade_status");
        if ("TRADE_SUCCESS".equals(tradeStatus)) {
            //alipay validation
            Map<String, String> params = new HashMap<>();
            Map<String, String[]> parameterMap = request.getParameterMap();
            for (String name : parameterMap.keySet()) {
                params.put(name, request.getParameter(name));
            }
            if (Factory.Payment.Common().verifyNotify(params)) {
                // 验证通过
                log.info("验证通过");
                String outTradeNo = params.get("out_trade_no");
                AlipayCallbackResult callbackResult = BeanUtil.toBean(params, AlipayCallbackResult.class);
                ResponseResult<?> responseResult = alipayService.alipayCallback(outTradeNo);
                log.info("调用订单服务结果：{}", responseResult.getData());
                log.info("params: {}", params);
                log.info("callbackResult: {}", callbackResult);
            } else {
                log.error("支付宝验证不通过");
            }
        }
        return "success";
    }
}

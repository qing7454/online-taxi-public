package com.qing.servicealipay.controller;

import com.qing.internalcommon.dto.ResponseResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@RequestMapping(path = "/alipay")
public class TestController {

    @PostMapping(path = "/test-notify")
    public ResponseResult<?> testAlipayCallback() {
        log.info("<===========alipay success callback==========>");
        return ResponseResult.success("alipay success");
    }
}

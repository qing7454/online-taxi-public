package com.qing.servicealipay.remote;

import com.qing.internalcommon.dto.ResponseResult;
import com.qing.internalcommon.request.OrderRequest;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient("service-order")
public interface ServiceOrderClient {

    @PostMapping(path = "/order/pay")
    ResponseResult<?> orderPaySuccess(@RequestBody OrderRequest orderRequest);

}

package com.qing.servicealipay.service;

import com.qing.internalcommon.dto.ResponseResult;
import com.qing.internalcommon.request.OrderRequest;
import com.qing.servicealipay.remote.ServiceOrderClient;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AlipayService {

    private final ServiceOrderClient serviceOrderClient;

    /**
     * 支付宝支付成功回调
     * @return
     */
    public ResponseResult<?> alipayCallback(String orderNo) {
        OrderRequest orderRequest = new OrderRequest();
        orderRequest.setOrderId(Long.parseLong(orderNo));
        return serviceOrderClient.orderPaySuccess(orderRequest);
    }
}

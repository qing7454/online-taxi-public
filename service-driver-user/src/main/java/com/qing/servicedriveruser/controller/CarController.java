package com.qing.servicedriveruser.controller;


import com.qing.internalcommon.dto.Car;
import com.qing.internalcommon.dto.ResponseResult;
import com.qing.servicedriveruser.service.ICarService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author fanrenqing
 * @since 2022-11-06
 */
@RestController
@RequestMapping("/car")
@RequiredArgsConstructor
public class CarController {

    private final ICarService carService;

    @PostMapping
    public ResponseResult<?> addCar(@RequestBody Car car) {
        return carService.addCar(car);
    }

    @GetMapping
    public ResponseResult<Car> getCarById(Long carId) {
        Car car = carService.getById(carId);
        return ResponseResult.success(car);
    }

}

package com.qing.servicedriveruser.controller;

import com.qing.internalcommon.dto.ResponseResult;
import com.qing.servicedriveruser.service.CityDriverUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/city-driver")
@RequiredArgsConstructor
public class CityDriverController {

    private final CityDriverUserService cityDriverUserService;

    @GetMapping(path = "/is-available-driver")
    public ResponseResult<Boolean> isAvailableDriver(String cityCode) {
        return cityDriverUserService.isAvailableDriver(cityCode);
    }
}

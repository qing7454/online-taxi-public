package com.qing.servicedriveruser.controller;


import com.qing.internalcommon.constant.DriverCarConstants;
import com.qing.internalcommon.dto.DriverCarBindingRelationship;
import com.qing.internalcommon.dto.ResponseResult;
import com.qing.servicedriveruser.service.IDriverCarBindingRelationshipService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author fanrenqing
 * @since 2022-11-08
 */
@RestController
@RequestMapping("/driver-car-binding-relationship")
@RequiredArgsConstructor
public class DriverCarBindingRelationshipController {

    private final IDriverCarBindingRelationshipService driverCarBindingRelationshipService;

    @PostMapping(path = "/bind")
    public ResponseResult<?> binding(@RequestBody DriverCarBindingRelationship relationship) {

        return driverCarBindingRelationshipService.bind(relationship);
    }

    @PostMapping(path = "/unbind")
    public ResponseResult<?> unbinding(@RequestBody DriverCarBindingRelationship relationship) {

        return driverCarBindingRelationshipService.unbind(relationship);
    }
}

package com.qing.servicedriveruser.controller;

import com.qing.internalcommon.dto.DriverUser;
import com.qing.internalcommon.dto.ResponseResult;
import com.qing.internalcommon.response.DriverUserExistsResponse;
import com.qing.internalcommon.response.OrderDriverResponse;
import com.qing.servicedriveruser.service.DriverUserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
@RequiredArgsConstructor
public class DriverUserController {

    private final DriverUserService driverUserService;

    /**
     * 新增司机
     * @param driverUser
     * @return
     */
    @PostMapping(path = "/user")
    public ResponseResult<?> addUser(@RequestBody DriverUser driverUser) {
        log.info("user info: {}", driverUser);
        return driverUserService.saveUser(driverUser);
    }

    /**
     * 修改司机
     * @param driverUser
     * @return
     */
    @PutMapping(path = "/user")
    public ResponseResult<?> updateUser(@RequestBody DriverUser driverUser) {
        return driverUserService.updateUser(driverUser);
    }

    /**
     * 查询 司机
     * 如果需要按照司机的多个条件做查询，那么此处用 /user
     * @return
     */
    @GetMapping("/check-driver/{driverPhone}")
    public ResponseResult<DriverUserExistsResponse> getDriverUser(@PathVariable("driverPhone") String driverPhone) {
        return driverUserService.getDriverUserByPhone(driverPhone);
    }

    /**
     * 根据车辆ID查找可用司机
     * @param carId 车辆ID
     * @return response
     */
    @GetMapping(path = "/get-available-driver/{carId}")
    public ResponseResult<?> getAvailableDriver(@PathVariable("carId") String carId) {
        return driverUserService.getAvailableDriver(carId);
    }
}

package com.qing.servicedriveruser.controller;


import com.qing.internalcommon.dto.DriverUserWorkStatus;
import com.qing.internalcommon.dto.ResponseResult;
import com.qing.servicedriveruser.service.IDriverUserWorkStatusService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author fanrenqing
 * @since 2022-11-06
 */
@RestController
@RequiredArgsConstructor
@Slf4j
@RequestMapping("/driver-user-work-status")
public class DriverUserWorkStatusController {

    private final IDriverUserWorkStatusService driverUserWorkStatusService;

    @PostMapping(path = "/change")
    public ResponseResult<?> changeWorkStatus(@RequestBody DriverUserWorkStatus driverUserWorkStatus) {
        Integer workStatus = driverUserWorkStatus.getWorkStatus();
        Long driverId = driverUserWorkStatus.getDriverId();
        log.info("driverId: {}, workStatus: {}", driverId, workStatus);

        return driverUserWorkStatusService.changeWorkStatus(driverId, workStatus);
    }
}

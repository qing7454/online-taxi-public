package com.qing.servicedriveruser.controller;

import com.qing.internalcommon.dto.ResponseResult;
import com.qing.servicedriveruser.service.DriverUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class TestController {

    private final DriverUserService driverUserService;

    @GetMapping
    public String test() {
        return "service-driver-user";
    }

    @GetMapping(path = "/test-db")
    public ResponseResult<?> testDB() {
        ResponseResult<?> responseResult = driverUserService.selectAll();
        System.out.println(responseResult.getData());
        return responseResult;
    }
}

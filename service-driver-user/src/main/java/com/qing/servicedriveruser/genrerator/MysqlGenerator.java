package com.qing.servicedriveruser.genrerator;

import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;

import java.util.Collections;

/**
 * 自动生成代码工具类
 */
public class MysqlGenerator {
    public static void main(String[] args) {

        FastAutoGenerator.create("jdbc:mysql://localhost:3306/service-driver-user?characterEncoding=utf-8&serverTimezone=GMT%2B8",
                "root","")
                .globalConfig(builder -> {
                    builder.author("fanrenqing").fileOverride().outputDir("/Users/xiumingfan/IdeaProjects/online-taxi-public/service-driver-user/src/main/java");
                })
                .packageConfig(builder -> {
                    builder.parent("com.qing.servicedriveruser").pathInfo(Collections.singletonMap(OutputFile.mapperXml,
                            "/Users/xiumingfan/IdeaProjects/online-taxi-public/service-driver-user/src/main/java/com/qing/servicedriveruser/mapper"));
                })
                .strategyConfig(builder -> {
                    builder.addInclude("driver_car_binding_relationship");

                })
                .templateEngine(new FreemarkerTemplateEngine())
                .execute();
    }
}
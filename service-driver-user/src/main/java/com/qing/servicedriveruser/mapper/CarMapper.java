package com.qing.servicedriveruser.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qing.internalcommon.dto.Car;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author fanrenqing
 * @since 2022-11-06
 */
public interface CarMapper extends BaseMapper<Car> {

}

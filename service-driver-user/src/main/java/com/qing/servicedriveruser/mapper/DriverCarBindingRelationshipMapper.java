package com.qing.servicedriveruser.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qing.internalcommon.dto.DriverCarBindingRelationship;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author fanrenqing
 * @since 2022-11-08
 */
public interface DriverCarBindingRelationshipMapper extends BaseMapper<DriverCarBindingRelationship> {

}

package com.qing.servicedriveruser.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qing.internalcommon.dto.DriverUser;
import com.qing.internalcommon.response.OrderDriverResponse;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author xiumingfan
 * @description 针对表【driver_user】的数据库操作Mapper
 * @createDate 2022-11-05 00:15:36
 */
@Repository
public interface DriverUserMapper extends BaseMapper<DriverUser> {

    int countDriverUserByCityCode(@Param("cityCode") String cityCode);

    List<OrderDriverResponse> selectAvailableDriver(@Param("carId") String carId, @Param("workStatus") Integer workStatus, @Param("bindState") Integer bindState);
}

package com.qing.servicedriveruser.remote;

import com.qing.internalcommon.dto.ResponseResult;
import com.qing.internalcommon.response.TerminalResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient("service-map")
public interface ServiceMapClient {

    @PostMapping(path = "/terminal/add")
    ResponseResult<?> addTerminal(@RequestParam("name") String name, @RequestParam("desc") String desc);

    @PostMapping(path = "/tract/add")
    ResponseResult<?> addTract(@RequestParam("tid") String tid);
}

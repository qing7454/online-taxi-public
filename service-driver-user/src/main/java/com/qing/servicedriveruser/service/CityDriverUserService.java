package com.qing.servicedriveruser.service;

import com.qing.internalcommon.dto.ResponseResult;
import com.qing.servicedriveruser.mapper.DriverUserMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class CityDriverUserService {

    private final DriverUserMapper driverUserMapper;

    public ResponseResult<Boolean> isAvailableDriver(String cityCode) {
        int driverCount = driverUserMapper.countDriverUserByCityCode(cityCode);
        return ResponseResult.success(driverCount > 0);
    }
}

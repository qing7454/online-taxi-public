package com.qing.servicedriveruser.service;

import com.qing.internalcommon.constant.CommonStatusEnum;
import com.qing.internalcommon.constant.DriverCarConstants;
import com.qing.internalcommon.dto.DriverUser;
import com.qing.internalcommon.dto.ResponseResult;
import com.qing.internalcommon.response.DriverUserExistsResponse;
import com.qing.internalcommon.response.OrderDriverResponse;
import com.qing.servicedriveruser.mapper.DriverUserMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Service
@RequiredArgsConstructor
public class DriverUserService {

    private final DriverUserMapper driverUserMapper;

    public ResponseResult<?> selectAll() {
        Map<String, Object> map = new HashMap<>();
        map.put("id", 1);
        List<DriverUser> driverUsers = driverUserMapper.selectByMap(map);
        return ResponseResult.success(driverUsers);
    }

    @Transactional
    public ResponseResult<?> saveUser(DriverUser driverUser) {
        LocalDateTime now = LocalDateTime.now();
        driverUser.setGmtCreate(now);
        driverUser.setGmtModified(now);
        int insert = driverUserMapper.insert(driverUser);
        return ResponseResult.success(insert);
    }

    public ResponseResult<?> updateUser(DriverUser driverUser) {
        LocalDateTime now = LocalDateTime.now();
        driverUser.setGmtModified(now);
        int update = driverUserMapper.updateById(driverUser);
        return ResponseResult.success(update);
    }

    public ResponseResult<DriverUserExistsResponse> getDriverUserByPhone(String driverPhone) {
        Map<String, Object> queryMap = new HashMap<>();
        queryMap.put("driver_phone", driverPhone);
        queryMap.put("state", DriverCarConstants.DRIVER_STATE_VALID);
        List<DriverUser> driverUsers = driverUserMapper.selectByMap(queryMap);
        DriverUserExistsResponse driverUserExistsResponse = new DriverUserExistsResponse();
        driverUserExistsResponse.setDriverPhone(driverPhone);
        driverUserExistsResponse.setExists(DriverCarConstants.DRIVER_NOT_EXISTS);
        if (driverUsers.isEmpty()) {
            return ResponseResult.success(driverUserExistsResponse);
        }

        DriverUser driverUser = driverUsers.get(0);
        if (Objects.equals(driverUser.getDriverPhone(), driverPhone)) {
            driverUserExistsResponse.setDriverPhone(driverUser.getDriverPhone());
            driverUserExistsResponse.setExists(DriverCarConstants.DRIVER_EXISTS);
        }
        return ResponseResult.success(driverUserExistsResponse);
    }

    public ResponseResult<?> getAvailableDriver(String carId) {
        List<OrderDriverResponse> orderDriverResponses = driverUserMapper.selectAvailableDriver(carId, DriverCarConstants.DRIVER_WORK_STATUS_START, DriverCarConstants.DRIVER_CAR_BIND);
        if (orderDriverResponses.isEmpty()) {
            return ResponseResult.fail(CommonStatusEnum.AVAILABLE_DRIVER_EMPTY);
        }
        return ResponseResult.success(orderDriverResponses.get(0));
    }
}

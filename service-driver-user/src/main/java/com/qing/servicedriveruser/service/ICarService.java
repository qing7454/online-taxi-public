package com.qing.servicedriveruser.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qing.internalcommon.dto.Car;
import com.qing.internalcommon.dto.ResponseResult;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author fanrenqing
 * @since 2022-11-06
 */
public interface ICarService extends IService<Car> {

    ResponseResult<?> addCar(Car car);

}

package com.qing.servicedriveruser.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qing.internalcommon.dto.DriverCarBindingRelationship;
import com.qing.internalcommon.dto.ResponseResult;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author fanrenqing
 * @since 2022-11-08
 */
public interface IDriverCarBindingRelationshipService extends IService<DriverCarBindingRelationship> {

    ResponseResult<String> bind(DriverCarBindingRelationship relationship);

    ResponseResult<?> unbind(DriverCarBindingRelationship relationship);

}

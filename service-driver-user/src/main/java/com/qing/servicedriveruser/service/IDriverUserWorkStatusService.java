package com.qing.servicedriveruser.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qing.internalcommon.dto.DriverUserWorkStatus;
import com.qing.internalcommon.dto.ResponseResult;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author fanrenqing
 * @since 2022-11-06
 */
public interface IDriverUserWorkStatusService extends IService<DriverUserWorkStatus> {

    ResponseResult<?> changeWorkStatus(Long driverId, Integer workStatus);

}

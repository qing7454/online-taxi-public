package com.qing.servicedriveruser.service.impl;

import com.qing.internalcommon.constant.CommonStatusEnum;
import com.qing.internalcommon.dto.Car;
import com.qing.internalcommon.dto.ResponseResult;
import com.qing.internalcommon.response.TerminalResponse;
import com.qing.servicedriveruser.mapper.CarMapper;
import com.qing.servicedriveruser.remote.ServiceMapClient;
import com.qing.servicedriveruser.service.ICarService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.LinkedHashMap;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author fanrenqing
 * @since 2022-11-06
 */
@Service
@RequiredArgsConstructor
public class CarServiceImpl extends ServiceImpl<CarMapper, Car> implements ICarService {

    private final ServiceMapClient serviceMapClient;

    @Override
    public ResponseResult<?> addCar(Car car) {
        LocalDateTime now = LocalDateTime.now();
        car.setGmtCreate(now);
        car.setGmtModified(now);
        // 获取tid
        try {
            ResponseResult<?> terminalResponse = serviceMapClient.addTerminal(car.getVehicleNo(), car.getId()+"");
            if (terminalResponse.getCode() == CommonStatusEnum.FAIL.getCode()) {
                return ResponseResult.fail(terminalResponse.getMessage());
            }
            LinkedHashMap<String, Object> response = (LinkedHashMap<String, Object>) terminalResponse.getData();
            Integer tid = (Integer)response.get("tid");
            car.setTid(tid+"");
        } catch (Exception e) {
            return ResponseResult.fail(e.getMessage());
        }

        // 获取轨迹ID：trid
        ResponseResult<?> responseResult = serviceMapClient.addTract(car.getTid());
        Object data = responseResult.getData();
        if (responseResult.getCode() == CommonStatusEnum.FAIL.getCode()) {
            return ResponseResult.fail(responseResult.getMessage());
        }
        LinkedHashMap<String, Object> response = (LinkedHashMap<String, Object>) data;
        String trid = (String)response.get("trid");
        String trname = (String)response.get("trname");
        car.setTrid(trid);
        if (StringUtils.isNotBlank(trname)) {
            car.setTrname(trname);
        }

        save(car);
        return ResponseResult.success();
    }
}

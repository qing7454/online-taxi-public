package com.qing.servicedriveruser.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.qing.internalcommon.constant.CommonStatusEnum;
import com.qing.internalcommon.constant.DriverCarConstants;
import com.qing.internalcommon.dto.DriverCarBindingRelationship;
import com.qing.internalcommon.dto.ResponseResult;
import com.qing.servicedriveruser.mapper.DriverCarBindingRelationshipMapper;
import com.qing.servicedriveruser.service.IDriverCarBindingRelationshipService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author fanrenqing
 * @since 2022-11-08
 */
@Service
public class DriverCarBindingRelationshipServiceImpl extends ServiceImpl<DriverCarBindingRelationshipMapper, DriverCarBindingRelationship> implements IDriverCarBindingRelationshipService {

    @Override
    public ResponseResult<String> bind(DriverCarBindingRelationship relationship) {
        // 绑定关系已存在
        LambdaQueryWrapper<DriverCarBindingRelationship> relationshipQuery = Wrappers.<DriverCarBindingRelationship>lambdaQuery()
                .eq(DriverCarBindingRelationship::getDriverId, relationship.getDriverId())
                .eq(DriverCarBindingRelationship::getCarId, relationship.getCarId())
                .eq(DriverCarBindingRelationship::getBindState, DriverCarConstants.DRIVER_CAR_BIND);
        DriverCarBindingRelationshipMapper relationshipMapper = getBaseMapper();
        Long driverCarBindingRelationships = relationshipMapper.selectCount(relationshipQuery);
        if (driverCarBindingRelationships > 0) {
            return ResponseResult.fail(CommonStatusEnum.DRIVER_CAR_BIND_EXISTS);
        }
        // 司机已被绑定
        LambdaQueryWrapper<DriverCarBindingRelationship> driverQuery = Wrappers.<DriverCarBindingRelationship>lambdaQuery()
                .eq(DriverCarBindingRelationship::getDriverId, relationship.getDriverId())
                .eq(DriverCarBindingRelationship::getBindState, DriverCarConstants.DRIVER_CAR_BIND);
        Long driverRelationships = relationshipMapper.selectCount(driverQuery);
        if (driverRelationships > 0) {
            return ResponseResult.fail(CommonStatusEnum.DRIVER_BIND_EXISTS);
        }

        // 车辆已被绑定
        LambdaQueryWrapper<DriverCarBindingRelationship> carQuery = Wrappers.<DriverCarBindingRelationship>lambdaQuery()
                .eq(DriverCarBindingRelationship::getCarId, relationship.getCarId())
                .eq(DriverCarBindingRelationship::getBindState, DriverCarConstants.DRIVER_CAR_BIND);
        Long carRelationships = relationshipMapper.selectCount(carQuery);
        if (carRelationships > 0) {
            return ResponseResult.fail(CommonStatusEnum.CAR_BIND_EXISTS);
        }

        LocalDateTime now = LocalDateTime.now();
        relationship.setBindingTime(now);
        relationship.setBindState(DriverCarConstants.DRIVER_CAR_BIND);
        save(relationship);
        return ResponseResult.success();
    }

    @Override
    public ResponseResult<?> unbind(DriverCarBindingRelationship relationship) {
        LambdaQueryWrapper<DriverCarBindingRelationship> relationshipQuery = Wrappers.<DriverCarBindingRelationship>lambdaQuery()
                .eq(DriverCarBindingRelationship::getDriverId, relationship.getDriverId())
                .eq(DriverCarBindingRelationship::getCarId, relationship.getCarId())
                .eq(DriverCarBindingRelationship::getBindState, DriverCarConstants.DRIVER_CAR_BIND);
        DriverCarBindingRelationshipMapper relationshipMapper = getBaseMapper();
        List<DriverCarBindingRelationship> driverCarBindingRelationships = relationshipMapper.selectList(relationshipQuery);
        if (driverCarBindingRelationships.isEmpty()) {
            return ResponseResult.fail(CommonStatusEnum.DRIVER_CAR_BIND_NOT_EXISTS);
        }

        DriverCarBindingRelationship driverCarBindingRelationship = driverCarBindingRelationships.get(0);
        LocalDateTime now = LocalDateTime.now();
        driverCarBindingRelationship.setUnBindingTime(now);
        driverCarBindingRelationship.setBindState(DriverCarConstants.DRIVER_CAR_UNBIND);
        relationshipMapper.updateById(driverCarBindingRelationship);
        return ResponseResult.success();
    }
}

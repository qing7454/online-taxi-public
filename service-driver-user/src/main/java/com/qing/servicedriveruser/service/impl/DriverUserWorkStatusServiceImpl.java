package com.qing.servicedriveruser.service.impl;

import com.qing.internalcommon.dto.DriverUserWorkStatus;
import com.qing.internalcommon.dto.ResponseResult;
import com.qing.servicedriveruser.mapper.DriverUserWorkStatusMapper;
import com.qing.servicedriveruser.service.IDriverUserWorkStatusService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author fanrenqing
 * @since 2022-11-06
 */
@Service
public class DriverUserWorkStatusServiceImpl extends ServiceImpl<DriverUserWorkStatusMapper, DriverUserWorkStatus> implements IDriverUserWorkStatusService {

    @Override
    public ResponseResult<?> changeWorkStatus(Long driverId, Integer workStatus) {
        Map<String,Object> queryMap = new HashMap<>();
        queryMap.put("driver_id", driverId);
        List<DriverUserWorkStatus> driverUserWorkStatuses = getBaseMapper().selectByMap(queryMap);
        DriverUserWorkStatus driverUserWorkStatus = driverUserWorkStatuses.get(0);

        driverUserWorkStatus.setWorkStatus(workStatus);

        updateById(driverUserWorkStatus);
        return ResponseResult.success();
    }
}

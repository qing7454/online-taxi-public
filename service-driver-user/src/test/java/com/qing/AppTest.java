package com.qing;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.io.*;
import java.nio.file.Files;

/**
 * Unit test for simple App.
 */
public class AppTest extends TestCase {

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest(String testName) throws Exception {
        super(testName);
        InputStream in = Files.newInputStream(new File("/Users/xiumingfan/IdeaProjects/online-taxi-public/service-driver-user/src/main/java/com/qing/servicedriveruser/ServiceDriverUserApplication.java").toPath());
        BufferedInputStream bis = new BufferedInputStream(in);
        int available = in.available();
        byte[] buf = new byte[available];
        int read = bis.read(buf);
        System.out.println(new String(buf, 0, read));
        bis.close();
        in.close();
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(AppTest.class);
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp() {
        assertTrue(true);
    }
}

package com.qing.servicemap.controller;

import com.qing.internalcommon.dto.ForecastPriceDTO;
import com.qing.internalcommon.dto.ResponseResult;
import com.qing.servicemap.service.DirectionService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/direction")
@RequiredArgsConstructor
public class DirectionController {

    private final DirectionService directionService;

    @GetMapping(path = "/driving")
    public ResponseResult<?> driving(@RequestBody ForecastPriceDTO forecastPriceDTO) {
        String depLongitude = forecastPriceDTO.getDepLongitude();
        String depLatitude = forecastPriceDTO.getDepLatitude();
        String destLongitude = forecastPriceDTO.getDestLongitude();
        String destLatitude = forecastPriceDTO.getDestLatitude();
        return directionService.driving(depLongitude, depLatitude, destLongitude, destLatitude);
    }
}

package com.qing.servicemap.controller;

import com.qing.internalcommon.dto.ResponseResult;
import com.qing.internalcommon.request.PointRequest;
import com.qing.servicemap.service.PointService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/point")
@RequiredArgsConstructor
public class PointController {

    private final PointService pointService;

    @PostMapping(path = "/upload")
    public ResponseResult<?> upload(@RequestBody PointRequest pointRequest) {
        return pointService.upload(pointRequest);
    }
}

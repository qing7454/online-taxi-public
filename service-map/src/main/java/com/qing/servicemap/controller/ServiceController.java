package com.qing.servicemap.controller;

import com.qing.internalcommon.dto.ResponseResult;
import com.qing.servicemap.service.ServiceFromMapService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/service")
public class ServiceController {

    private final ServiceFromMapService serviceFromMapService;

    @PostMapping(path = "/add")
    public ResponseResult<?> addService(String name) {
        return serviceFromMapService.add(name);
    }
}

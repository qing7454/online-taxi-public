package com.qing.servicemap.controller;

import com.qing.internalcommon.dto.ResponseResult;
import com.qing.servicemap.service.TerminalService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@Slf4j
@RequestMapping(path = "/terminal")
public class TerminalController {

    private final TerminalService terminalService;

    @PostMapping(path = "/add")
    public ResponseResult<?> add(String name, String desc) {
        log.info("终端名称: {}, carId: {}", name, desc);
        if (StringUtils.isBlank(name)) {
            return ResponseResult.fail("终端名称不能为空");
        }
        return terminalService.add(name, desc);
    }

    @PostMapping(path = "/around-search")
    public ResponseResult<?> aroundSearch(String center, String radius) {
        return terminalService.aroundSearch(center, radius);
    }

    /**
     * 终端轨迹查询 598083842 1670338972925 1670339599400
     * @param tid 终端ID
     * @param starttime 开始时间
     * @param endtime 结束时间
     * @return result
     */
    @PostMapping(path = "/trsearch")
    public ResponseResult<?> trsearch(String tid, Long starttime, Long endtime) {
        return terminalService.trsearch(tid, starttime, endtime);
    }
}

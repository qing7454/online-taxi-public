package com.qing.servicemap.controller;

import com.qing.servicemap.mapper.DictDistrictMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequiredArgsConstructor
public class TestController {

    private final DictDistrictMapper dictDistrictMapper;

    @GetMapping
    public String test() {
        return "service-map";
    }

    @GetMapping(path = "/test-map")
    public String testMap() {
        Map<String, Object> map = new HashMap<>();
        map.put("address_code", "110000");
        dictDistrictMapper.selectByMap(map);
        return "teest-map";
    }
}

package com.qing.servicemap.controller;

import com.qing.internalcommon.dto.ResponseResult;
import com.qing.servicemap.service.TractService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/tract")
@RequiredArgsConstructor
public class TractController {

    private final TractService tractService;

    @PostMapping(path = "/add")
    public ResponseResult<?> addTract(String tid) {
        return tractService.add(tid);
    }
}

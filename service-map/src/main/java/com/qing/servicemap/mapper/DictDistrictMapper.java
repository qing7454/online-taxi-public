package com.qing.servicemap.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qing.internalcommon.dto.DictDistrict;
import org.springframework.stereotype.Repository;

@Repository
public interface DictDistrictMapper extends BaseMapper<DictDistrict> {

}

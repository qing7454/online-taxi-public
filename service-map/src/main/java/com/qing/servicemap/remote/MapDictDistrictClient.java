package com.qing.servicemap.remote;

import com.qing.internalcommon.constant.AmapConfigConstants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Slf4j
@Service
public class MapDictDistrictClient {


    @Value("${amap.key}")
    private String key;

    @Autowired
    private RestTemplate restTemplate;

    public String initDictDistrict(String keywords) {
        StringBuilder url = new StringBuilder(AmapConfigConstants.DISTRICT_URL);
        url.append("?");
        url.append("keywords=").append(keywords);
        url.append("&");
        url.append("subdistrict=3");
        url.append("&");
        url.append("key=").append(key);
        log.info("url: {}", url);

        return restTemplate.getForEntity(url.toString(), String.class).getBody();
    }
}

package com.qing.servicemap.remote;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.qing.internalcommon.constant.AmapConfigConstants;
import com.qing.internalcommon.response.DirectionResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
@Slf4j
public class MapDirectionClient {

    @Value("${amap.key}")
    private String key;

    @Autowired
    private RestTemplate restTemplate;

    /**
     * 路径规划服务
     * @param depLongitude 起始经度
     * @param depLatitude 起始纬度
     * @param destLongitude 目标经度
     * @param destLatitude 目标纬度
     * @return result
     */
    public DirectionResponse direction(String depLongitude, String depLatitude, String destLongitude, String destLatitude) {

        /*
        https://restapi.amap.com/v3/direction/driving?
        origin=116.481028,39.989643&destination=116.465302,40.004717
        &extensions=all&output=json&key=b0cbcac8ca1bbb20cef79a73dc006d8b
         */
        StringBuilder urlBuilder = new StringBuilder(AmapConfigConstants.DIRECTION_URL);
        urlBuilder.append("?");
        urlBuilder.append("origin=").append(depLongitude).append(",").append(depLatitude);
        urlBuilder.append("&");
        urlBuilder.append("destination=").append(destLongitude).append(",").append(destLatitude);
        urlBuilder.append("&");
        urlBuilder.append("extensions=base");
        urlBuilder.append("&");
        urlBuilder.append("output=json");
        urlBuilder.append("&");
        urlBuilder.append("key=").append(key);
        log.info("url: {}", urlBuilder);
        // 调用高德地图驾车路径规划接口
        ResponseEntity<String> directionEntity = restTemplate.getForEntity(urlBuilder.toString(), String.class);
        log.info("map response direction entity: {}", directionEntity);
        // 解析接口数据
        DirectionResponse directionResponse = parseDirectionEntity(directionEntity.getBody());
        return directionResponse;
    }

    private DirectionResponse parseDirectionEntity(String source) {
        DirectionResponse directionResponse = new DirectionResponse();
        try {
            JSONObject jsonObject = JSON.parseObject(source);
            if (jsonObject.containsKey(AmapConfigConstants.STATUS_KEY)) {
                Integer status = jsonObject.getInteger(AmapConfigConstants.STATUS_KEY);
                if (1 == status) {
                    if (jsonObject.containsKey(AmapConfigConstants.ROUTE_KEY)) {
                        JSONObject routeObject = jsonObject.getJSONObject(AmapConfigConstants.ROUTE_KEY);
                        JSONArray pathsArray = routeObject.getJSONArray(AmapConfigConstants.PATHS_KEY);
                        JSONObject pathObject = pathsArray.getJSONObject(0);
                        if (pathObject.containsKey(AmapConfigConstants.DISTANCE_KEY)) {
                            String distance = pathObject.getString(AmapConfigConstants.DISTANCE_KEY);
                            String duration = pathObject.getString(AmapConfigConstants.DURATION_KEY);
                            directionResponse.setDistance(Long.valueOf(distance));
                            directionResponse.setDuration(Long.valueOf(duration));
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return directionResponse;
    }
}

package com.qing.servicemap.remote;

import com.qing.internalcommon.constant.AmapConfigConstants;
import com.qing.internalcommon.dto.ResponseResult;
import com.qing.internalcommon.request.PointDTO;
import com.qing.internalcommon.request.PointRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URI;

@Service
public class PointClient {


    @Value("${amap.key}")
    private String key;

    @Value("${amap.sid}")
    private String sid;

    @Autowired
    private RestTemplate restTemplate;

    public ResponseResult<?> upload(PointRequest pointRequest) {
        String uri = AmapConfigConstants.POINT_UPLOAD_URL;
        // &key=<用户的key>
        // 拼装请求的url
        StringBuilder url = new StringBuilder(uri);
        url.append("?");
        url.append("key=").append(key);
        url.append("&");
        url.append("sid=").append(sid);
        url.append("&");
        url.append("tid=").append(pointRequest.getTid());
        url.append("&");
        url.append("trid=").append(pointRequest.getTrid());
        url.append("&");
        url.append("points=");
        PointDTO[] points = pointRequest.getPoints();
        url.append("%5B");
        for (PointDTO p : points) {
            url.append("%7B");
            String locatetime = p.getLocatetime();
            String location = p.getLocation();
            url.append("%22location%22");
            url.append("%3A");
            url.append("%22").append(location).append("%22");
            url.append("%2C");

            url.append("%22locatetime%22");
            url.append("%3A");
            url.append(locatetime);

            url.append("%7D");
        }
        url.append("%5D");

        System.out.println("上传位置请求："+url);
        ResponseEntity<String> stringResponseEntity = restTemplate.postForEntity(URI.create(url.toString()), null, String.class);
        System.out.println("上传位置响应："+stringResponseEntity.getBody());
        //restTemplate.postForEntity(url, pointRequest, String.class);
        return ResponseResult.success();
    }
}

package com.qing.servicemap.remote;

import com.qing.internalcommon.constant.AmapConfigConstants;
import com.qing.internalcommon.dto.ResponseResult;
import com.qing.internalcommon.response.AmapServiceResponse;
import com.qing.internalcommon.response.ServiceResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class ServiceClient {

    @Value("${amap.key}")
    private String key;

    @Autowired
    private RestTemplate restTemplate;

    public ResponseResult<?> addService(String name) {
        String url = String.format(AmapConfigConstants.SERVICE_ADD_URL, key, name);
        ResponseEntity<AmapServiceResponse> response = restTemplate.postForEntity(url, null, AmapServiceResponse.class);
        AmapServiceResponse body = response.getBody();
        assert body != null;
        if (AmapConfigConstants.SERVICE_ERROR == body.getErrcode()) {
            return ResponseResult.fail(body.getErrdetail().toString());
        }
        AmapServiceResponse.AmapServiceData data = body.getData();
        Integer sid = data.getSid();
        ServiceResponse serviceResponse = new ServiceResponse();
        serviceResponse.setSid(sid);
        return ResponseResult.success(serviceResponse);
    }
}

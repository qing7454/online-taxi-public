package com.qing.servicemap.remote;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.qing.internalcommon.constant.AmapConfigConstants;
import com.qing.internalcommon.dto.ResponseResult;
import com.qing.internalcommon.response.AmapServiceResponse;
import com.qing.internalcommon.response.TerminalResponse;
import com.qing.internalcommon.response.TrsearchResponse;
import com.qing.internalcommon.response.amap.TrSearchData;
import com.qing.internalcommon.response.amap.TrSearchResult;
import com.qing.internalcommon.response.amap.Track;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class TerminalClient {


    @Value("${amap.key}")
    private String key;

    @Value("${amap.sid}")
    private String sid;

    @Autowired
    private RestTemplate restTemplate;

    public ResponseResult<?> add(String name, String desc) {
        String url = String.format(AmapConfigConstants.TERMINAL_ADD_URL, key, sid, name, desc);
        ResponseEntity<AmapServiceResponse> response = restTemplate.postForEntity(url, null, AmapServiceResponse.class);
        AmapServiceResponse body = response.getBody();
        if (null != body) {
            Integer errcode = body.getErrcode();
            if (AmapConfigConstants.SERVICE_SUCCESS == errcode) {
                AmapServiceResponse.AmapServiceData data = body.getData();
                Integer tid = data.getTid();
                TerminalResponse terminalResponse = new TerminalResponse();
                terminalResponse.setTid(tid+"");
                return ResponseResult.success(terminalResponse);

            }
        }
        assert body != null;
        return ResponseResult.fail(body.getErrdetail().toString());
    }

    public ResponseResult<?> aroundSearch(String center, String radius) {
        String url = String.format(AmapConfigConstants.TERMINAL_AROUND_SEARCH_URL, key, sid, center, radius);
        ResponseEntity<String> aroundSearchResponse = restTemplate.postForEntity(url, null, String.class);

        String body = aroundSearchResponse.getBody();
        log.info("高德地图周边搜索返回数据：{}", JSON.parseObject(body));
        // 解析终端搜索结果
        JSONObject result = JSONObject.parseObject(body);
        assert result != null;
        JSONObject data = result.getJSONObject("data");

        List<TerminalResponse> terminalResponseList = new ArrayList<>();

        JSONArray results = data.getJSONArray("results");
        for (int i=0;i<results.size();i++){
            TerminalResponse terminalResponse = new TerminalResponse();

            JSONObject jsonObject = results.getJSONObject(i);
            // desc是carId
            String desc = jsonObject.getString("desc");
            Long carId = Long.parseLong(desc);
            String tid = jsonObject.getString("tid");

            JSONObject location = jsonObject.getJSONObject("location");
            String longitude = location.getString("longitude");
            String latitude = location.getString("latitude");

            terminalResponse.setCarId(carId);
            terminalResponse.setTid(tid);
            terminalResponse.setLongitude(longitude);
            terminalResponse.setLatitude(latitude);

            terminalResponseList.add(terminalResponse);
        }

        return ResponseResult.success(terminalResponseList);
    }

    public ResponseResult<?> trsearch(String tid, Long starttime, Long endtime) {
        String url = String.format(AmapConfigConstants.TERMINAL_TRSEARCH_URL, key, sid, tid, starttime, endtime);
        ResponseEntity<TrSearchResult> response = restTemplate.postForEntity(url, null, TrSearchResult.class);
        TrSearchResult body = response.getBody();
        assert body != null;
        TrSearchData data = body.getData();
        List<Track> tracks = data.getTracks();
        long driveMile = 0L;
        long driveTime = 0L;
        for (Track track : tracks) {
            Long distance = track.getDistance();
            Long time = track.getTime();
            time = time / (1000 * 60);
            driveMile = driveMile + distance;
            driveTime = driveTime + time;
        }
        log.info("终端轨迹查询结果：{}", body.getData());
        TrsearchResponse trsearchResponse = new TrsearchResponse();
        trsearchResponse.setDriveMile(driveMile);
        trsearchResponse.setDriveTime(driveTime);
        return ResponseResult.success(trsearchResponse);
    }
}

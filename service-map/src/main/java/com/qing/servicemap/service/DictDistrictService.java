package com.qing.servicemap.service;

import com.alibaba.fastjson2.JSON;
import com.qing.internalcommon.dto.DictDistrict;
import com.qing.internalcommon.dto.DistrictDTO;
import com.qing.internalcommon.dto.ResponseResult;
import com.qing.servicemap.mapper.DictDistrictMapper;
import com.qing.servicemap.remote.MapDictDistrictClient;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
@Slf4j
@RequiredArgsConstructor
public class DictDistrictService {

    private final MapDictDistrictClient mapDictDistrictClient;

    private final DictDistrictMapper dictDistrictMapper;

    private final String parentCode = "0";

    public ResponseResult<?> dicDistrict(String keywords) {
        String district = mapDictDistrictClient.initDictDistrict(keywords);
        //log.info("district: {}", district);
        // 解析json数据
        DistrictDTO districtDTO = JSON.to(DistrictDTO.class, district);
        if (Objects.equals("1", districtDTO.getStatus())) {
            List<DistrictDTO.DistrictItemDTO> districts = districtDTO.getDistricts();
            List<DictDistrict> dictDistricts = new ArrayList<>();
            flatDistrictData(districts, dictDistricts, parentCode);
            for (DictDistrict dictDistrict : dictDistricts) {
                dictDistrictMapper.insert(dictDistrict);
            }
        }
        log.info("保存地区数据成功！");

        return ResponseResult.success("init success");
    }

    /**
     * 将数据展开为一个list结构
     * @param districts 树状结构数据
     * @return list 平铺后的数据
     */
    private void flatDistrictData(List<DistrictDTO.DistrictItemDTO> districts, List<DictDistrict> dictDistricts, String pCode) {
        for (DistrictDTO.DistrictItemDTO district : districts) {
            DictDistrict dictDistrict = new DictDistrict();
            dictDistrict.setAddressCode(district.getAdcode());
            dictDistrict.setAddressName(district.getName());
            dictDistrict.setParentAddressCode(pCode);
            dictDistrict.setLevel(getLevel(district.getLevel()));
            dictDistricts.add(dictDistrict);
            if (!CollectionUtils.isEmpty(district.getDistricts())) {
                flatDistrictData(district.getDistricts(), dictDistricts, district.getAdcode());
            }
        }
    }

    private Integer getLevel(String district) {
        int levelName;
        switch (district) {
            case "province":
                levelName = 1;
                break;
            case "city":
                levelName = 2;
                break;
            case "district":
                levelName = 3;
                break;
            default:
                levelName = 0;
                break;
        }
        return levelName;
    }

}

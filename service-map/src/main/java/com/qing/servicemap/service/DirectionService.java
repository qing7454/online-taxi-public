package com.qing.servicemap.service;

import com.qing.internalcommon.dto.ResponseResult;
import com.qing.internalcommon.response.DirectionResponse;
import com.qing.servicemap.remote.MapDirectionClient;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class DirectionService {

    private final MapDirectionClient mapDirectionClient;

    /**
     * 根据起点经纬度和终点经纬度获取距离（米）和时长（分钟）
     * @param depLongitude 起点经度
     * @param depLatitude 起点纬度
     * @param destLongitude 终点经度
     * @param destLatitude 终点纬度
     * @return 距离 时长
     */
    public ResponseResult<?> driving(String depLongitude, String depLatitude, String destLongitude, String destLatitude) {

        //调用地图接口
        DirectionResponse direction = mapDirectionClient.direction(depLongitude, depLatitude, destLongitude, destLatitude);
        return ResponseResult.success(direction);
    }
}

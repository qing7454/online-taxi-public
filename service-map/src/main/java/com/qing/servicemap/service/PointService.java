package com.qing.servicemap.service;

import com.qing.internalcommon.dto.ResponseResult;
import com.qing.internalcommon.request.PointRequest;
import com.qing.servicemap.remote.PointClient;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PointService {

    private final PointClient pointClient;

    public ResponseResult<?> upload(PointRequest pointRequest) {
        return pointClient.upload(pointRequest);
    }
}

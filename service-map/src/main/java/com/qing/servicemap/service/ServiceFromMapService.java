package com.qing.servicemap.service;

import com.qing.internalcommon.dto.ResponseResult;
import com.qing.servicemap.remote.ServiceClient;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ServiceFromMapService {

    private final ServiceClient serviceClient;

    public ResponseResult<?> add(String name) {
        return serviceClient.addService(name);
    }
}

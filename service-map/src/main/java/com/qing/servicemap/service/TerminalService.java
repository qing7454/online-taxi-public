package com.qing.servicemap.service;

import com.qing.internalcommon.dto.ResponseResult;
import com.qing.servicemap.remote.TerminalClient;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class TerminalService {

    private final TerminalClient terminalClient;

    public ResponseResult<?> add(String name, String desc) {
        return terminalClient.add(name, desc);
    }

    public ResponseResult<?> aroundSearch(String center, String radius) {
        return terminalClient.aroundSearch(center, radius);
    }

    public ResponseResult<?> trsearch(String tid, Long starttime, Long endtime) {
        return terminalClient.trsearch(tid, starttime, endtime);
    }
}

package com.qing.servicemap.service;

import com.qing.internalcommon.dto.ResponseResult;
import com.qing.servicemap.remote.TractClient;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class TractService {

    private final TractClient tractClient;

    public ResponseResult<?> add(String tid) {
        return tractClient.add(tid);
    }
}

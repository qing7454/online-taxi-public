package com.qing.serviceorder.config;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RedisConfig {

    @Value("${spring.redis.host}")
    private String redisHost;
    @Value("${spring.redis.port}")
    private int redisPort;
    private final String protocol = "redis://";

    @Bean
    public RedissonClient redisClient() {
        Config config = new Config();
        String address = String.format("%s%s:%d", protocol, redisHost, redisPort);
        config.useSingleServer().setAddress(address).setDatabase(1);
        return Redisson.create(config);
    }
}

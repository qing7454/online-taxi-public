package com.qing.serviceorder.controller;

import com.qing.internalcommon.constant.HeaderParamConstants;
import com.qing.internalcommon.dto.ResponseResult;
import com.qing.internalcommon.request.OrderRequest;
import com.qing.serviceorder.service.IOrderInfoService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(path = "/order")
@Slf4j
@RequiredArgsConstructor
public class OrderController {

    private final IOrderInfoService orderInfoService;

    /**
     * 乘客下订单
     * @param orderRequest
     * @param request
     * @return
     */
    @PostMapping(path = "/add")
    public ResponseResult<?> add(@RequestBody OrderRequest orderRequest, HttpServletRequest request) {
        String deviceCode = request.getHeader(HeaderParamConstants.DEVICE_CODE);
        //orderRequest.setDeviceCode(deviceCode);
        log.info("order info: {}", orderRequest);
        return orderInfoService.add(orderRequest);
    }

    /**
     * 出发去接乘客
     * @param orderRequest
     * @return
     */
    @PostMapping(path = "/to-pickup-passenger")
    public ResponseResult<?> toPickupPassenger(@RequestBody OrderRequest orderRequest) {
        return orderInfoService.toPickupPassenger(orderRequest);
    }

    /**
     * 到达乘客上车点
     * @param orderRequest
     * @return
     */
    @PostMapping(path = "/arrived-departure")
    public ResponseResult<?> arrivedDeparture(@RequestBody OrderRequest orderRequest) {
        return orderInfoService.arrivedDeparture(orderRequest);
    }

    /**
     * 接到乘客
     * @param orderRequest
     * @return
     */
    @PostMapping(path = "/pickup-passenger")
    public ResponseResult<?> pickupPassenger(@RequestBody OrderRequest orderRequest) {
        return orderInfoService.pickupPassenger(orderRequest);
    }

    /**
     * 乘客到达目的地下车
     * @param orderRequest 订单
     * @return result
     */
    @PostMapping(path = "/passenger-get-off")
    public ResponseResult<?> passengerGetOff(@RequestBody OrderRequest orderRequest) {
        return orderInfoService.passengerGetOff(orderRequest);
    }

    /**
     * 订单支付
     * @param orderRequest 订单ID
     * @return result
     */
    @PostMapping(path = "/pay")
    public ResponseResult<?> pay(@RequestBody OrderRequest orderRequest) {
        return orderInfoService.pay(orderRequest);
    }

    /**
     * 取消订单
     * @param orderId 订单ID
     * @param identity 取消人身份 1、乘客 2、司机
     * @return
     */
    @PostMapping(path = "/cancel")
    public ResponseResult<?> cancel(@RequestParam String orderId, @RequestParam String identity) {
        return orderInfoService.cancel(orderId, identity);
    }
}

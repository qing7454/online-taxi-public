package com.qing.serviceorder.controller;

import com.qing.internalcommon.dto.OrderInfo;
import com.qing.serviceorder.service.IOrderInfoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class TestController {

    @Autowired
    private IOrderInfoService orderInfoService;

    @GetMapping
    public String test() {
        return "service-order";
    }

    @GetMapping(path = "/test-real-time-order/{orderId}")
    public String testRealTimeOrder(@PathVariable("orderId") Long orderId) {
        log.info("测试并发订单orderId：{}", orderId);
        OrderInfo orderInfo = orderInfoService.findByOrderId(orderId);
        orderInfoService.dispatchRealTimeOrder(orderInfo);
        return "test-real-time-order success.";
    }
}

package com.qing.serviceorder.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qing.internalcommon.dto.OrderInfo;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author fanrenqing
 * @since 2022-11-14
 */
public interface OrderInfoMapper extends BaseMapper<OrderInfo> {

}

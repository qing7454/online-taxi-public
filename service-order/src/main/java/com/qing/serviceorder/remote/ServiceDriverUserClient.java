package com.qing.serviceorder.remote;

import com.qing.internalcommon.dto.Car;
import com.qing.internalcommon.dto.ResponseResult;
import com.qing.internalcommon.response.OrderDriverResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient("service-driver-user")
public interface ServiceDriverUserClient {

    @GetMapping(path = "/city-driver/is-available-driver")
    ResponseResult<Boolean> isCityDriverAvailable(@RequestParam String cityCode);

    @GetMapping(path = "/get-available-driver/{carId}")
    ResponseResult<OrderDriverResponse> getAvailableDriver(@PathVariable Long carId);

    @GetMapping(path = "/car")
    ResponseResult<Car> getCarById(@RequestParam Long carId);
}

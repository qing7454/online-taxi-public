package com.qing.serviceorder.remote;

import com.qing.internalcommon.dto.PriceRule;
import com.qing.internalcommon.dto.ResponseResult;
import com.qing.internalcommon.request.CalculatePriceRequest;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.math.BigDecimal;

@FeignClient("service-price")
public interface ServicePriceClient {

    @GetMapping(path = "/price-rule/is-newest")
    ResponseResult<?> isNewest(@RequestParam String fareType, @RequestParam Integer fareVersion);

    @PostMapping(path = "/price-rule/if-exists")
    ResponseResult<Boolean> ifPriceRuleExists(@RequestBody PriceRule priceRule);

    @PostMapping(path = "/calculate-price")
    ResponseResult<BigDecimal> calculatePrice(@RequestBody CalculatePriceRequest calculatePriceRequest);
}

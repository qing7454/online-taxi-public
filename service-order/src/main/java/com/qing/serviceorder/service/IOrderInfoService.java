package com.qing.serviceorder.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qing.internalcommon.dto.OrderInfo;
import com.qing.internalcommon.dto.ResponseResult;
import com.qing.internalcommon.request.OrderRequest;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author fanrenqing
 * @since 2022-11-14
 */
public interface IOrderInfoService extends IService<OrderInfo> {

    ResponseResult<?> add(OrderRequest orderRequest);

    OrderInfo findByOrderId(Long orderId);

    int dispatchRealTimeOrder(OrderInfo orderInfo);

    ResponseResult<?> toPickupPassenger(OrderRequest orderRequest);

    ResponseResult<?> arrivedDeparture(OrderRequest orderRequest);

    ResponseResult<?> pickupPassenger(OrderRequest orderRequest);

    ResponseResult<?> passengerGetOff(OrderRequest orderRequest);

    ResponseResult<?> pay(OrderRequest orderRequest);

    ResponseResult<?> cancel(String orderId, String identity);

}

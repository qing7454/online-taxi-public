package com.qing.serviceorder.service.impl;

import com.alibaba.fastjson2.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qing.internalcommon.constant.CommonStatusEnum;
import com.qing.internalcommon.constant.IdentityConstant;
import com.qing.internalcommon.constant.OrderConstants;
import com.qing.internalcommon.dto.Car;
import com.qing.internalcommon.dto.OrderInfo;
import com.qing.internalcommon.dto.PriceRule;
import com.qing.internalcommon.dto.ResponseResult;
import com.qing.internalcommon.request.CalculatePriceRequest;
import com.qing.internalcommon.request.OrderRequest;
import com.qing.internalcommon.response.OrderDriverResponse;
import com.qing.internalcommon.response.TerminalResponse;
import com.qing.internalcommon.response.TrsearchResponse;
import com.qing.internalcommon.util.RedisPrefixUtil;
import com.qing.serviceorder.mapper.OrderInfoMapper;
import com.qing.serviceorder.remote.ServiceDriverUserClient;
import com.qing.serviceorder.remote.ServiceMapClient;
import com.qing.serviceorder.remote.ServicePriceClient;
import com.qing.serviceorder.remote.ServiceSsePushClient;
import com.qing.serviceorder.service.IOrderInfoService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.BeanUtils;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author fanrenqing
 * @since 2022-11-14
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class OrderInfoServiceImpl extends ServiceImpl<OrderInfoMapper, OrderInfo> implements IOrderInfoService {

    private final ServicePriceClient servicePriceClient;

    private final StringRedisTemplate stringRedisTemplate;

    private final ServiceDriverUserClient serviceDriverUserClient;

    private final ServiceMapClient serviceMapClient;

    private final RedissonClient redissonClient;

    private final ServiceSsePushClient serviceSsePushClient;

    /**
     * 添加订单
     * @param orderRequest 订单信息
     * @return result
     */
    @Override
    public ResponseResult<?> add(OrderRequest orderRequest) {
        String fareType = orderRequest.getFareType();
        Integer fareVersion = orderRequest.getFareVersion();

        // 当前城市是否有可用司机
        ResponseResult<Boolean> cityDriverAvailable = serviceDriverUserClient.isCityDriverAvailable(orderRequest.getAddress());
        log.info("当前城市是否有可用司机：{}", cityDriverAvailable.getData());
        if (!cityDriverAvailable.getData()) {
            return ResponseResult.fail(CommonStatusEnum.CITY_DRIVER_EMPTY);
        }

        // 是否为最新计价规则
        ResponseResult<?> newest = servicePriceClient.isNewest(fareType, fareVersion);
        if (CommonStatusEnum.FAIL.getCode() == newest.getCode()) {
            return ResponseResult.fail(newest.getMessage());
        }
        Boolean isNew = (Boolean)newest.getData();
        if (isNew == null) {
            return ResponseResult.fail(newest.getCode(), newest.getMessage());
        }
        if (!isNew) {
            return ResponseResult.fail(CommonStatusEnum.PRICE_RULE_CHANGED);
        }
        // 黑名单设备
        String deviceCode = orderRequest.getDeviceCode();
        if (isBlackDevice(deviceCode)) return ResponseResult.fail(CommonStatusEnum.DEVICE_IS_BLACK);

        // 判断计价规则是否存在
        if (!priceRuleExists(orderRequest)) {
            return ResponseResult.fail(CommonStatusEnum.CITY_SERVICE_NOT_SERVICE);
        }

        // 判断乘客是否有正在进行的订单
        if (passengerHasRunningOrder(orderRequest.getPassengerId())) {
            return ResponseResult.fail(CommonStatusEnum.ORDER_GOING_ON);
        }

        // 创建订单
        OrderInfo orderInfo = new OrderInfo();
        BeanUtils.copyProperties(orderRequest, orderInfo);
        orderInfo.setOrderStatus(OrderConstants.ORDER_START);
        LocalDateTime now = LocalDateTime.now();
        orderInfo.setGmtCreate(now);
        orderInfo.setGmtModified(now);
        save(orderInfo);

        for (int i = 0; i < 6; i++) {
            // 派单
            int dispatchResult = dispatchRealTimeOrder(orderInfo);
            if (dispatchResult == 1) {
                break;
            }
            if (5 == i) {
                orderInfo.setOrderStatus(OrderConstants.ORDER_INVALID);
                updateById(orderInfo);
            } else {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        }
        return ResponseResult.success();
    }

    /**
     * 实时订单派单
     * @param orderInfo 订单参数
     */
    public int dispatchRealTimeOrder(OrderInfo orderInfo) {
        int result = 0;
        String lat = orderInfo.getDepLatitude();
        String lon = orderInfo.getDepLongitude();
        String center = String.format("%s,%s", lat, lon);
//        int radius = 2000;
        int[] radiusArr = new int[]{2000, 4000, 5000};
        ResponseResult<List<TerminalResponse>> responseResult = null;
        radiusFlag:
        for (int radius : radiusArr) {
            responseResult = serviceMapClient.aroundSearch(center, radius);
            List<TerminalResponse> data = responseResult.getData();
            log.info("在半径范围 {} 内搜寻车辆，寻找车辆,结果：{}", radius, responseResult);
            // 获取终端
            for (TerminalResponse datum : data) {
                Long carId = datum.getCarId();
                ResponseResult<OrderDriverResponse> orderDriverResponse = serviceDriverUserClient.getAvailableDriver(carId);
                int code = orderDriverResponse.getCode();
                if (CommonStatusEnum.SUCCESS.getCode() == code) {
                    log.info("找到可用的司机：{}", orderDriverResponse);
                    OrderDriverResponse driverResponse = orderDriverResponse.getData();
                    Long driverId = driverResponse.getDriverId();
                    Long carId1 = driverResponse.getCarId();
                    String driverPhone = driverResponse.getDriverPhone();
                    String lockKey = (driverId+"").intern();
                    RLock lock = redissonClient.getLock(lockKey);
                    lock.lock();
                    if (driverHasRunningOrder(driverId)) {
                        lock.unlock();
                        log.info("司机ID为 {} 的有正在执行的订单: carId:{}, driverPhone:{}", driverId, carId1, driverPhone);
                    } else {
                        // 更新订单信息
                        updateOrderInfo(orderInfo, driverResponse, datum);
                        // 通知司机
                        notifyDriver(orderInfo);
                        // 通知乘客
                        notifyPassenger(orderInfo);
                        lock.unlock();
                        result = 1;
                        break radiusFlag;
                    }
                } else {
                    log.info("没找到司机：{}", orderDriverResponse.getMessage());
                    continue radiusFlag;
                }
            }
        }
        return result;
    }

    /**
     * 根据订单ID查询订单记录
     * @param orderId 订单ID
     * @return 订单
     */
    public OrderInfo findByOrderId(Long orderId) {
        return getById(orderId);
    }

    /**
     * 司机前往乘客点
     * @param orderRequest order info
     * @return result
     */
    @Override
    public ResponseResult<?> toPickupPassenger(OrderRequest orderRequest) {
        String toPickUpPassengerAddress = orderRequest.getToPickUpPassengerAddress();
        String toPickUpPassengerLongitude = orderRequest.getToPickUpPassengerLongitude();
        String toPickUpPassengerLatitude = orderRequest.getToPickUpPassengerLatitude();
        LocalDateTime toPickUpPassengerTime = LocalDateTime.now();
        // 查询订单信息
        Long orderId = orderRequest.getOrderId();
        OrderInfo orderInfo = getById(orderId);
        if (null == orderInfo) {
            return ResponseResult.fail(CommonStatusEnum.ORDER_NOT_EXISTS);
        }
        orderInfo.setToPickUpPassengerAddress(toPickUpPassengerAddress);
        orderInfo.setToPickUpPassengerLongitude(toPickUpPassengerLongitude);
        orderInfo.setToPickUpPassengerLatitude(toPickUpPassengerLatitude);
        orderInfo.setToPickUpPassengerTime(toPickUpPassengerTime);
        orderInfo.setOrderStatus(OrderConstants.DRIVER_TO_PICK_UP_PASSENGER);
        updateById(orderInfo);
        return ResponseResult.success();
    }

    /**
     * 到达乘客上车点
     * @param orderRequest
     * @return
     */
    @Override
    public ResponseResult<?> arrivedDeparture(OrderRequest orderRequest) {
        Long orderId = orderRequest.getOrderId();
        OrderInfo orderInfo = getById(orderId);
        if (null == orderInfo) {
            return ResponseResult.fail(CommonStatusEnum.ORDER_NOT_EXISTS);
        }
        orderInfo.setOrderStatus(OrderConstants.DRIVER_ARRIVED_DEPARTURE);
        orderInfo.setDriverArrivedDepartureTime(LocalDateTime.now());
        updateById(orderInfo);
        return ResponseResult.success();
    }

    /**
     * 到达乘客上车点接到乘客上车
     * @param orderRequest
     * @return
     */
    @Override
    public ResponseResult<?> pickupPassenger(OrderRequest orderRequest) {
        Long orderId = orderRequest.getOrderId();
        OrderInfo orderInfo = getById(orderId);
        if (null == orderInfo) {
            return ResponseResult.fail(CommonStatusEnum.ORDER_NOT_EXISTS);
        }
        orderInfo.setPickUpPassengerLongitude(orderRequest.getPickUpPassengerLongitude());
        orderInfo.setPickUpPassengerLatitude(orderRequest.getPickUpPassengerLatitude());
        orderInfo.setPickUpPassengerTime(LocalDateTime.now());
        orderInfo.setOrderStatus(OrderConstants.PICK_UP_PASSENGER);
        updateById(orderInfo);
        return ResponseResult.success();
    }

    /**
     * 乘客下车
     * @param orderRequest
     * @return
     */
    @Override
    public ResponseResult<?> passengerGetOff(OrderRequest orderRequest) {
        Long orderId = orderRequest.getOrderId();
        OrderInfo orderInfo = getById(orderId);
        orderInfo.setPassengerGetoffLongitude(orderRequest.getPassengerGetoffLongitude());
        orderInfo.setPassengerGetoffLatitude(orderRequest.getPassengerGetoffLatitude());
        orderInfo.setPassengerGetoffTime(LocalDateTime.now());
        orderInfo.setOrderStatus(OrderConstants.PASSENGER_GETOFF);
        // 订单行驶的里程数和公里数
        ResponseResult<Car> carInfo = serviceDriverUserClient.getCarById(orderInfo.getCarId());
        String tid = carInfo.getData().getTid();
        Long starttime = orderInfo.getPickUpPassengerTime().toInstant(ZoneOffset.of("+8")).toEpochMilli();
        Long endtime = LocalDateTime.now().toInstant(ZoneOffset.of("+8")).toEpochMilli();
        ResponseResult<TrsearchResponse> trsearch = serviceMapClient.trsearch(tid, starttime, endtime);
        TrsearchResponse data = trsearch.getData();
        Long driveMile = data.getDriveMile();
        Long driveTime = data.getDriveTime();
        orderInfo.setDriveMile(driveMile);
        orderInfo.setDriveTime(driveTime);
        // 获取价格
        CalculatePriceRequest calculatePriceRequest = new CalculatePriceRequest();
        calculatePriceRequest.setDuration(driveTime);
        calculatePriceRequest.setDistance(driveMile);
        String address = orderInfo.getAddress();
        String vehicleNo = orderInfo.getVehicleNo();
        calculatePriceRequest.setCityCode(address);
        calculatePriceRequest.setVehicleType(vehicleNo);
        ResponseResult<BigDecimal> priceResponseResult = servicePriceClient.calculatePrice(calculatePriceRequest);
        BigDecimal price = priceResponseResult.getData();
        orderInfo.setPrice(price);
        updateById(orderInfo);
        return ResponseResult.success();
    }

    /**
     * 支付订单
     * @param orderRequest
     * @return
     */
    @Override
    public ResponseResult<?> pay(OrderRequest orderRequest) {
        OrderInfo orderInfo = getById(orderRequest.getOrderId());
        if (null == orderInfo) {
            return ResponseResult.fail(CommonStatusEnum.ORDER_NOT_EXISTS);
        }
        orderInfo.setOrderStatus(OrderConstants.SUCCESS_PAY);
        updateById(orderInfo);
        return ResponseResult.success();
    }

    /**
     * 取消订单
     * @param orderId 订单ID
     * @param identity 取消人身份 1、乘客 2、司机
     * @return result
     */
    @Override
    public ResponseResult<?> cancel(String orderId, String identity) {
        OrderInfo orderInfo = getById(Long.valueOf(orderId));
        if (null == orderInfo) {
            return ResponseResult.fail(CommonStatusEnum.ORDER_NOT_EXISTS);
        }
        LocalDateTime receiveOrderTime = orderInfo.getReceiveOrderTime();
        Integer orderStatus = orderInfo.getOrderStatus();
        int cancelOperator = Integer.parseInt(identity);
        LocalDateTime cancelTime = LocalDateTime.now();
        int cancelTypeCode = 0;
        boolean cancelValidate = true;
        // 乘客取消
        if (Objects.equals(IdentityConstant.PASSENGER_IDENTITY, identity)) {
            switch (orderStatus) {
                // 订单开始
                case OrderConstants.ORDER_START:
                    cancelTypeCode = OrderConstants.CANCEL_PASSENGER_BEFORE;
                    break;
                // 司机接到订单
                case OrderConstants.DRIVER_RECEIVE_ORDER:
                    long between = ChronoUnit.MINUTES.between(receiveOrderTime, cancelTime);
                    if (2 < between) {
                        cancelTypeCode = OrderConstants.CANCEL_PASSENGER_ILLEGAL;
                    } else {
                        cancelTypeCode = OrderConstants.CANCEL_PASSENGER_BEFORE;
                    }
                    break;
                case OrderConstants.DRIVER_ARRIVED_DEPARTURE:
                case OrderConstants.DRIVER_TO_PICK_UP_PASSENGER:
                    cancelTypeCode = OrderConstants.CANCEL_PASSENGER_ILLEGAL;
                    break;
                default:
                    log.info("乘客取消订单失败");
                    cancelValidate = false;
                    break;
            }
        }
        // 司机取消订单
        if (Objects.equals(IdentityConstant.DRIVER_IDENTITY, identity)) {
            switch (orderStatus) {
                // 订单开始
                // 司机接到乘客
                case OrderConstants.DRIVER_RECEIVE_ORDER:
                case OrderConstants.DRIVER_TO_PICK_UP_PASSENGER:
                case OrderConstants.DRIVER_ARRIVED_DEPARTURE:
                    long between = ChronoUnit.MINUTES.between(receiveOrderTime, cancelTime);
                    if (between > 1){
                        cancelTypeCode = OrderConstants.CANCEL_DRIVER_ILLEGAL;
                    }else {
                        cancelTypeCode = OrderConstants.CANCEL_DRIVER_BEFORE;
                    }
                    break;
                default:
                    log.info("司机取消失败");
                    cancelValidate = false;
                    break;
            }
        }

        if (!cancelValidate) {
            return ResponseResult.fail(CommonStatusEnum.ORDER_CANCEL_ERROR);
        }

        orderInfo.setOrderStatus(OrderConstants.ORDER_CANCEL);
        orderInfo.setCancelOperator(cancelOperator);
        orderInfo.setCancelTime(cancelTime);
        orderInfo.setCancelTypeCode(cancelTypeCode);
        updateById(orderInfo);
        return ResponseResult.success();
    }

    // ======================================以下为内部调用方法=======================================
    /**
     * 派单更新订单
     * @param orderInfo 订单信息
     * @param driverResponse 司机信息
     * @param terminal 接单终端，车辆
     */
    private void updateOrderInfo(OrderInfo orderInfo, OrderDriverResponse driverResponse, TerminalResponse terminal) {
        String driverPhone = driverResponse.getDriverPhone();
        String licenseId = driverResponse.getLicenseId();
        String vehicleNo = driverResponse.getVehicleNo();
        Long driverId = driverResponse.getDriverId();
        Long carId = driverResponse.getCarId();
        orderInfo.setDriverId(driverId);
        orderInfo.setCarId(carId);
        orderInfo.setDriverPhone(driverPhone);
        orderInfo.setVehicleNo(vehicleNo);
        orderInfo.setLicenseId(licenseId);
        orderInfo.setOrderStatus(OrderConstants.DRIVER_RECEIVE_ORDER);
        orderInfo.setReceiveOrderTime(LocalDateTime.now());
        // from map service
        orderInfo.setReceiveOrderCarLongitude(terminal.getLongitude());
        orderInfo.setReceiveOrderCarLatitude(terminal.getLatitude());
        getBaseMapper().updateById(orderInfo);
    }

    /**
     * 通知司机接单成功
     * @param orderInfo
     */
    private void notifyDriver(OrderInfo orderInfo) {
        Long driverId = orderInfo.getDriverId();
        JSONObject driverContent = new  JSONObject();

        driverContent.put("orderId",orderInfo.getId());
        driverContent.put("passengerId",orderInfo.getPassengerId());
        driverContent.put("passengerPhone",orderInfo.getPassengerPhone());
        driverContent.put("departure",orderInfo.getDeparture());
        driverContent.put("depLongitude",orderInfo.getDepLongitude());
        driverContent.put("depLatitude",orderInfo.getDepLatitude());

        driverContent.put("destination",orderInfo.getDestination());
        driverContent.put("destLongitude",orderInfo.getDestLongitude());
        driverContent.put("destLatitude",orderInfo.getDestLatitude());
        String msg = driverContent.toString();
        serviceSsePushClient.push(driverId, msg, IdentityConstant.DRIVER_IDENTITY);
    }

    /**
     * 通知乘客接单成功
     * @param orderInfo
     */
    private void notifyPassenger(OrderInfo orderInfo) {
        // 通知乘客
        JSONObject passengerContent = new  JSONObject();
        passengerContent.put("orderId",orderInfo.getId());
        passengerContent.put("driverId",orderInfo.getDriverId());
        passengerContent.put("driverPhone",orderInfo.getDriverPhone());
        passengerContent.put("vehicleNo",orderInfo.getVehicleNo());
        Long carId = orderInfo.getCarId();
        // 车辆信息，调用车辆服务
        ResponseResult<Car> carById = serviceDriverUserClient.getCarById(carId);
        Car carRemote = carById.getData();

        passengerContent.put("brand", carRemote.getBrand());
        passengerContent.put("model",carRemote.getModel());
        passengerContent.put("vehicleColor",carRemote.getVehicleColor());

        passengerContent.put("receiveOrderCarLongitude",orderInfo.getReceiveOrderCarLongitude());
        passengerContent.put("receiveOrderCarLatitude",orderInfo.getReceiveOrderCarLatitude());

        serviceSsePushClient.push(orderInfo.getPassengerId(), IdentityConstant.PASSENGER_IDENTITY, passengerContent.toString());
    }

    /**
     * 判断计价规则是否存在
     * @param orderRequest
     * @return
     */
    private boolean priceRuleExists(OrderRequest orderRequest) {
        String fareType = orderRequest.getFareType();
        String[] cityCodeAndVehicleType = fareType.split("\\$");
        String cityCode = cityCodeAndVehicleType[0];
        String vehicleType = cityCodeAndVehicleType[1];
        PriceRule priceRule = new PriceRule();
        priceRule.setCityCode(cityCode);
        priceRule.setVehicleType(vehicleType);
        ResponseResult<Boolean> responseResult = servicePriceClient.ifPriceRuleExists(priceRule);
        return responseResult.getData();
    }

    /**
     * 判断是否黑名单中的设备
     * @param deviceCode 设备唯一吗
     * @return 是否为黑名单设备
     */
    private boolean isBlackDevice(String deviceCode) {
        String deviceCodeKey = String.format(RedisPrefixUtil.blackDeviceCodePrefix, deviceCode);
        Boolean hasKey = stringRedisTemplate.hasKey(deviceCodeKey);
        if (Boolean.TRUE.equals(hasKey)) {
            String deviceCodeRedis = stringRedisTemplate.opsForValue().get(deviceCodeKey);
            assert deviceCodeRedis != null;
            int codeRedisNum = Integer.parseInt(deviceCodeRedis);
            if (codeRedisNum > 1) {
                return true;
            } else {
                stringRedisTemplate.opsForValue().increment(deviceCodeKey);
            }
        } else {
            stringRedisTemplate.opsForValue().setIfAbsent(deviceCodeKey, "1", 1L, TimeUnit.HOURS);
        }
        return false;
    }

    /**
     * 判断是否有正在进行的订单
     * @param passengerId 乘客ID
     * @return boolean
     */
    private Boolean passengerHasRunningOrder(Long passengerId) {
        LambdaQueryWrapper<OrderInfo> orderQuery = Wrappers.<OrderInfo>lambdaQuery().eq(OrderInfo::getPassengerId, passengerId)
                .and(query -> query.ge(OrderInfo::getOrderStatus, OrderConstants.ORDER_START)
                        .le(OrderInfo::getOrderStatus, OrderConstants.TO_START_PAY));
        Long count = getBaseMapper().selectCount(orderQuery);

        return count > 0;
    }

    /**
     * 判断实际是否有正在运行的订单
     * @param driverId 司机ID
     * @return bool
     */
    private Boolean driverHasRunningOrder(Long driverId) {
        LambdaQueryWrapper<OrderInfo> queryWrapper = Wrappers.<OrderInfo>lambdaQuery()
                .eq(OrderInfo::getDriverId, driverId)
                .and(query -> query.eq(OrderInfo::getOrderStatus, OrderConstants.DRIVER_RECEIVE_ORDER)
                        .or().eq(OrderInfo::getOrderStatus, OrderConstants.DRIVER_TO_PICK_UP_PASSENGER)
                        .or().eq(OrderInfo::getOrderStatus, OrderConstants.DRIVER_ARRIVED_DEPARTURE)
                        .or().eq(OrderInfo::getOrderStatus, OrderConstants.PICK_UP_PASSENGER));
        Long count = getBaseMapper().selectCount(queryWrapper);
        log.info("司机ID：{}, 正在执行订单数：{}", driverId, count);
        return count > 0;
    }

}

package com.qing.servicepassengeruser.controller;

import com.qing.internalcommon.dto.ResponseResult;
import com.qing.internalcommon.request.VerificationCodeDTO;
import com.qing.servicepassengeruser.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @PostMapping(path = "/user")
    public ResponseResult<?> signInOrSignUp(@RequestBody VerificationCodeDTO verificationCodeDTO) {
        log.info("user info: {}", verificationCodeDTO);
        return userService.signInOrSignUp(verificationCodeDTO.getPassengerPhone());
    }

    @GetMapping(path = "/user/{phone}")
    public ResponseResult<?> getUser(@PathVariable("phone") String phone) {
        log.info("get user info phone: {}", phone);
        return userService.getUserInfo(phone);
    }
}

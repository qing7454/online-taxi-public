package com.qing.servicepassengeruser.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qing.internalcommon.dto.PassengerUser;
import org.springframework.stereotype.Repository;

@Repository
public interface PassengerUserMapper extends BaseMapper<PassengerUser> {
}

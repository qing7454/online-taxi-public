package com.qing.servicepassengeruser.service;

import cn.hutool.core.util.RandomUtil;
import com.qing.internalcommon.constant.CommonStatusEnum;
import com.qing.internalcommon.dto.PassengerUser;
import com.qing.internalcommon.dto.ResponseResult;
import com.qing.servicepassengeruser.mapper.PassengerUserMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
@RequiredArgsConstructor
public class UserService {

    private final PassengerUserMapper passengerUserMapper;

    public ResponseResult<?> signInOrSignUp(String passengerPhone) {
        log.info("user phone: {}", passengerPhone);
        // 根据手机号查询用户
        Map<String, Object> map = new HashMap<>();
        map.put("passenger_phone", passengerPhone);
        List<PassengerUser> passengerUsers = passengerUserMapper.selectByMap(map);
        log.info("passengerUsers: {}", passengerUsers);

        // 判断用户是否存在
        if (passengerUsers.isEmpty()) {
            // 如果用户不存在，插入用户信息
            PassengerUser passengerUser = new PassengerUser();
            String userSuffix = RandomUtil.randomString(8);
            passengerUser.setPassengerName("用户"+userSuffix);
            passengerUser.setPassengerPhone(passengerPhone);
            passengerUser.setPassengerGender(Byte.valueOf("0"));
            passengerUser.setState(Byte.valueOf("0"));
            LocalDateTime now = LocalDateTime.now();
            passengerUser.setCreateTime(now);
            passengerUser.setUpdateTime(now);
            passengerUserMapper.insert(passengerUser);
        }

        return ResponseResult.success("");
    }

    public ResponseResult<?> getUserInfo(String phone) {
        Map<String, Object> map = new HashMap<>();
        map.put("passenger_phone", phone);
        List<PassengerUser> passengerUsers = passengerUserMapper.selectByMap(map);
        if (passengerUsers.isEmpty()) {
            return ResponseResult.fail(CommonStatusEnum.USER_NOT_EXISTS.getCode(), CommonStatusEnum.USER_NOT_EXISTS.getValue());
        }

        PassengerUser passengerUser = passengerUsers.get(0);

        return ResponseResult.success(passengerUser);
    }
}

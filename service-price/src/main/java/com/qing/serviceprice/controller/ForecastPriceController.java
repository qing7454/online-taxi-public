package com.qing.serviceprice.controller;

import com.qing.internalcommon.dto.ForecastPriceDTO;
import com.qing.internalcommon.dto.ResponseResult;
import com.qing.internalcommon.request.CalculatePriceRequest;
import com.qing.serviceprice.service.ForecastPriceService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class ForecastPriceController {

    private final ForecastPriceService forecastPriceService;

    @PostMapping(path = "/forecast-price")
    public ResponseResult<?> forecastPrice(@RequestBody ForecastPriceDTO forecastPriceDTO) {

        String depLongitude = forecastPriceDTO.getDepLongitude();
        String depLatitude = forecastPriceDTO.getDepLatitude();
        String destLongitude = forecastPriceDTO.getDestLongitude();
        String destLatitude = forecastPriceDTO.getDestLatitude();
        String cityCode = forecastPriceDTO.getCityCode();
        String vehicleType = forecastPriceDTO.getVehicleType();
        return forecastPriceService.forecastPrice(depLongitude, depLatitude, destLongitude, destLatitude, cityCode, vehicleType);
    }

    /**
     * 计算行程价格
     * @param calculatePriceRequest
     * @return
     */
    @PostMapping(path = "/calculate-price")
    public ResponseResult<?> calculatePrice(CalculatePriceRequest calculatePriceRequest) {
        return forecastPriceService.calculatePrice(calculatePriceRequest);
    }
}

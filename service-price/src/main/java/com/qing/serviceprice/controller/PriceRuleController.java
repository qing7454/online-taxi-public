package com.qing.serviceprice.controller;

import com.qing.internalcommon.dto.PriceRule;
import com.qing.internalcommon.dto.ResponseResult;
import com.qing.serviceprice.service.PriceRuleService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
@RequiredArgsConstructor
@RequestMapping(path = "/price-rule")
public class PriceRuleController {

    private final PriceRuleService priceRuleService;

    @PostMapping(path = "/add")
    public ResponseResult<?> add(@RequestBody PriceRule rule) {
        log.info("rule: {}", rule);
        return priceRuleService.add(rule);
    }

    @PostMapping(path = "/edit")
    public ResponseResult<?> edit(@RequestBody PriceRule priceRule) {
        return priceRuleService.edit(priceRule);
    }

    @GetMapping(path = "/get-newest-version")
    public ResponseResult<?> getNewestVersion(@RequestParam String fareType) {
        if (StringUtils.isBlank(fareType)) {
            String msg = resolveNotEmptyMsg("fareType");
            return ResponseResult.fail(msg);
        }
        return priceRuleService.getNewestVersion(fareType);
    }

    @GetMapping(path = "/is-newest")
    public ResponseResult<?> getNewest(@RequestParam String fareType, @RequestParam Integer fareVersion) {
        if (StringUtils.isBlank(fareType)) {
            return ResponseResult.fail(resolveNotEmptyMsg("fareType"));
        }
        if (StringUtils.isBlank(fareType)) {
            return ResponseResult.fail(resolveNotEmptyMsg("fareVersion"));
        }
        return priceRuleService.isNewest(fareType, fareVersion);
    }

    @PostMapping(path = "/if-exists")
    public ResponseResult<Boolean> ifExists(@RequestBody PriceRule priceRule) {
        return priceRuleService.ifExists(priceRule);
    }

    private String resolveNotEmptyMsg(String paramName) {
        return String.format("参数%s不能为空", paramName);
    }

}

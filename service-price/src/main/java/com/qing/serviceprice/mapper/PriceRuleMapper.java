package com.qing.serviceprice.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qing.internalcommon.dto.PriceRule;
import org.springframework.stereotype.Repository;

@Repository
public interface PriceRuleMapper extends BaseMapper<PriceRule> {
}

package com.qing.serviceprice.remote;

import com.qing.internalcommon.dto.ForecastPriceDTO;
import com.qing.internalcommon.dto.ResponseResult;
import com.qing.internalcommon.response.DirectionResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient("service-map")
public interface ServiceMapClient {

    @GetMapping(path = "/direction/driving")
    ResponseResult<DirectionResponse> getDirection(@RequestBody ForecastPriceDTO forecastPriceDTO);
}

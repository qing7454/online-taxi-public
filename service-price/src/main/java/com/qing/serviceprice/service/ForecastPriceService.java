package com.qing.serviceprice.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.qing.internalcommon.constant.CommonStatusEnum;
import com.qing.internalcommon.dto.ForecastPriceDTO;
import com.qing.internalcommon.dto.PriceRule;
import com.qing.internalcommon.dto.ResponseResult;
import com.qing.internalcommon.request.CalculatePriceRequest;
import com.qing.internalcommon.response.DirectionResponse;
import com.qing.internalcommon.response.ForecastPriceResponse;
import com.qing.serviceprice.mapper.PriceRuleMapper;
import com.qing.serviceprice.remote.ServiceMapClient;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class ForecastPriceService {

    private final ServiceMapClient serviceMapClient;

    private final PriceRuleMapper priceRuleMapper;

    /**
     * 计算预估价格
     * @param depLongitude 起始位置经度
     * @param depLatitude 起始位置纬度
     * @param destLongitude 目的地经度
     * @param destLatitude 目的地纬度
     * @return 价格
     */
    public ResponseResult<?> forecastPrice(String depLongitude, String depLatitude,
                                           String destLongitude, String destLatitude,
                                           String cityCode, String vehicleType) {
        log.info("出发地经度-depLongitude: {}", depLongitude);
        log.info("出发地纬度-depLatitude: {}", depLatitude);
        log.info("目的地经度-destLongitude: {}", destLongitude);
        log.info("目的地纬度-destLatitude: {}", destLatitude);
        log.info("城市编码-cityCode: {}", cityCode);
        log.info("车辆型号-cityCode: {}", vehicleType);

        log.info("调用地图服务，获取位置的距离和时长");
        ForecastPriceDTO forecastPriceDTO = new ForecastPriceDTO();
        forecastPriceDTO.setDepLongitude(depLongitude);
        forecastPriceDTO.setDepLatitude(depLatitude);
        forecastPriceDTO.setDestLongitude(destLongitude);
        forecastPriceDTO.setDestLatitude(destLatitude);
        forecastPriceDTO.setCityCode(cityCode);
        forecastPriceDTO.setVehicleType(vehicleType);
        ResponseResult<DirectionResponse> direction = serviceMapClient.getDirection(forecastPriceDTO);
        Long distance = direction.getData().getDistance();
        Long duration = direction.getData().getDuration();
        log.info("距离：{}，时长：{}", direction.getData().getDistance(), direction.getData().getDuration());

        log.info("读取计价规则");
        LambdaQueryWrapper<PriceRule> queryWrapper = Wrappers.<PriceRule>lambdaQuery()
                .eq(PriceRule::getCityCode, cityCode)
                .eq(PriceRule::getVehicleType, vehicleType)
                .orderByDesc(PriceRule::getFareVersion);
        List<PriceRule> priceRules = priceRuleMapper.selectList(queryWrapper);
        if (priceRules.isEmpty()) {
            return ResponseResult.fail(CommonStatusEnum.PRICE_RULE_EMPTY);
        }
        PriceRule priceRule = priceRules.get(0);
        log.info("price rule: {}", priceRule);

        log.info("根据距离、时长和计算规则，计算价格");
        BigDecimal price = getPrice(distance, duration, priceRule);

        ForecastPriceResponse forecastPriceResponse = new ForecastPriceResponse();
        forecastPriceResponse.setPrice(price);
        forecastPriceResponse.setCityCode(cityCode);
        forecastPriceResponse.setVehicleType(vehicleType);
        forecastPriceResponse.setFareType(priceRule.getFareType());
        forecastPriceResponse.setFareVersion(priceRule.getFareVersion());
        return ResponseResult.success(forecastPriceResponse);
    }


    /**
     * 计算价格
     * @param calculatePriceRequest
     * @return
     */
    public ResponseResult<?> calculatePrice(CalculatePriceRequest calculatePriceRequest) {
        String vehicleType = calculatePriceRequest.getVehicleType();
        String cityCode = calculatePriceRequest.getCityCode();
        Long distance = calculatePriceRequest.getDistance();
        Long duration = calculatePriceRequest.getDuration();
        LambdaQueryWrapper<PriceRule> queryWrapper = Wrappers.<PriceRule>lambdaQuery()
                .eq(PriceRule::getCityCode, cityCode)
                .eq(PriceRule::getVehicleType, vehicleType)
                .orderByDesc(PriceRule::getFareVersion);
        List<PriceRule> priceRules = priceRuleMapper.selectList(queryWrapper);
        if (priceRules.isEmpty()) {
            return ResponseResult.fail(CommonStatusEnum.PRICE_RULE_EMPTY);
        }
        PriceRule priceRule = priceRules.get(0);
        BigDecimal price = getPrice(distance, duration, priceRule);
        return ResponseResult.success(price);
    }

    /**
     * 根据距离、时长和计价规则，计算最终的价格
     * @param distance 距离
     * @param duration 时长
     * @param priceRule 计价规则
     * @return 价格
     */
    private static BigDecimal getPrice(Long distance, Long duration, PriceRule priceRule) {
        BigDecimal price = new BigDecimal("0");

        // 起步价
        Double startFare = priceRule.getStartFare();
        BigDecimal startFareDecimal = new BigDecimal(startFare);
        price = price.add(startFareDecimal);

        // 里程费
        Integer startMile = priceRule.getStartMile();
        // 实际的计算里程数
        BigDecimal distanceDecimal = new BigDecimal(distance).divide(new BigDecimal("1000"), 2, RoundingMode.HALF_UP);
        BigDecimal extractMile = distanceDecimal.subtract(new BigDecimal(startMile));
        BigDecimal extractMileDecimal = extractMile.compareTo(BigDecimal.ZERO) < 0 ? BigDecimal.ZERO : extractMile;
        //计程单价
        Double unitPricePerMile = priceRule.getUnitPricePerMile();
        BigDecimal unitPriceMileDecimal = new BigDecimal(unitPricePerMile);
        BigDecimal milePrice = extractMileDecimal.multiply(unitPriceMileDecimal);
        price = price.add(milePrice);

        // 计算时长费用
        // 实际分钟数
        BigDecimal timeMinute = BigDecimal.valueOf(duration).divide(new BigDecimal("60"), 2, RoundingMode.HALF_UP);
        // 每分钟费用
        BigDecimal unitPricePerMinute = BigDecimal.valueOf(priceRule.getUnitPricePerMinute());
        BigDecimal durationPrice = timeMinute.multiply(unitPricePerMinute);
        price = price.add(durationPrice);
        price = price.setScale(2, RoundingMode.HALF_UP);

        return price;
    }

//    public static void main(String[] args) {
//        PriceRule priceRule = new PriceRule();
//        priceRule.setUnitPricePerMile(1.8);
//        priceRule.setUnitPricePerMinute(0.5);
//        priceRule.setStartFare(10.0);
//        priceRule.setStartMile(3);
//
//        System.out.println(getPrice(6500, 1800, priceRule));
//    }
}

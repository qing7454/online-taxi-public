package com.qing.serviceprice.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.qing.internalcommon.constant.CommonStatusEnum;
import com.qing.internalcommon.dto.PriceRule;
import com.qing.internalcommon.dto.ResponseResult;
import com.qing.serviceprice.mapper.PriceRuleMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;

@Service
public class PriceRuleService {

    @Resource
    private PriceRuleMapper priceRuleMapper;

    public ResponseResult<?> add(PriceRule rule) {
        String cityCode = rule.getCityCode();
        String vehicleType = rule.getVehicleType();
        String fareType = cityCode + "$" + vehicleType;
        rule.setFareType(fareType);
        int fareVersion = 0;
        LambdaQueryWrapper<PriceRule> queryWrapper = Wrappers.<PriceRule>lambdaQuery()
                .eq(PriceRule::getCityCode, cityCode)
                        .eq(PriceRule::getVehicleType, vehicleType)
                                .orderByDesc(PriceRule::getFareVersion);
        List<PriceRule> priceRules = priceRuleMapper.selectList(queryWrapper);
        if (!priceRules.isEmpty()) {
            return ResponseResult.fail(CommonStatusEnum.PRICE_RULE_EXISTS);
        }
        rule.setFareVersion(++fareVersion);
        priceRuleMapper.insert(rule);
        return ResponseResult.success();
    }

    public ResponseResult<?> edit(PriceRule priceRule){
        // 拼接fareType
        String cityCode = priceRule.getCityCode();
        String vehicleType = priceRule.getVehicleType();
        String fareType = cityCode + "$" + vehicleType;
        priceRule.setFareType(fareType);

        // 添加版本号
        // 问题1：增加了版本号，前面的两个字段无法唯一确定一条记录，问题2：找最大的版本号，需要排序
        LambdaQueryWrapper<PriceRule> queryWrapper = Wrappers.<PriceRule>lambdaQuery()
                .eq(PriceRule::getCityCode, cityCode)
                .eq(PriceRule::getVehicleType, vehicleType)
                .orderByDesc(PriceRule::getFareVersion);
        List<PriceRule> priceRules = priceRuleMapper.selectList(queryWrapper);
        Integer fareVersion = 0;
        if (priceRules.size()>0){
            PriceRule lasterPriceRule = priceRules.get(0);
            Double unitPricePerMile = lasterPriceRule.getUnitPricePerMile();
            Double unitPricePerMinute = lasterPriceRule.getUnitPricePerMinute();
            Double startFare = lasterPriceRule.getStartFare();
            Integer startMile = lasterPriceRule.getStartMile();

            if (unitPricePerMile.doubleValue() == priceRule.getUnitPricePerMile().doubleValue()
                    && unitPricePerMinute.doubleValue() == priceRule.getUnitPricePerMinute().doubleValue()
                    && startFare.doubleValue() == priceRule.getStartFare().doubleValue()
                    && startMile.intValue() == priceRule.getStartMile().intValue()){
                return ResponseResult.fail(CommonStatusEnum.PRICE_RULE_NOT_EDIT);
            }


            fareVersion = lasterPriceRule.getFareVersion();
        }
        priceRule.setFareVersion(++fareVersion);


        priceRuleMapper.insert(priceRule);
        return ResponseResult.success();
    }

    public ResponseResult<?> getNewestVersion(String fareType) {
        LambdaQueryWrapper<PriceRule> queryWrapper = Wrappers.<PriceRule>lambdaQuery()
                .eq(PriceRule::getFareType, fareType)
                .orderByDesc(PriceRule::getFareVersion);
        List<PriceRule> priceRules = priceRuleMapper.selectList(queryWrapper);
        if (priceRules.isEmpty()) {
            return ResponseResult.fail(CommonStatusEnum.PRICE_RULE_EMPTY);
        }
        return ResponseResult.success(priceRules.get(0));
    }

    public ResponseResult<?> isNewest(String fareType, Integer fareVersion) {
        LambdaQueryWrapper<PriceRule> queryWrapper = Wrappers.<PriceRule>lambdaQuery()
                .eq(PriceRule::getFareType, fareType)
                .orderByDesc(PriceRule::getFareVersion);
        List<PriceRule> priceRules = priceRuleMapper.selectList(queryWrapper);
        if (priceRules.isEmpty()) {
            return ResponseResult.fail(CommonStatusEnum.PRICE_RULE_EMPTY);
        }
        PriceRule priceRule = priceRules.get(0);
        Integer fareVersionDb = priceRule.getFareVersion();
        if (Objects.equals(fareVersionDb, fareVersion)) {
            return ResponseResult.success(true);
        }
        return ResponseResult.success(false);
    }

    public ResponseResult<Boolean> ifExists(PriceRule priceRule) {
        String cityCode = priceRule.getCityCode();
        String vehicleType = priceRule.getVehicleType();
        LambdaQueryWrapper<PriceRule> queryWrapper = Wrappers.<PriceRule>lambdaQuery()
                .eq(PriceRule::getCityCode, cityCode)
                .eq(PriceRule::getVehicleType, vehicleType)
                .orderByDesc(PriceRule::getFareVersion);
        List<PriceRule> priceRules = priceRuleMapper.selectList(queryWrapper);

        return ResponseResult.success(!priceRules.isEmpty());
    }
}

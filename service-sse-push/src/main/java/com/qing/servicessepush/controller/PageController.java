package com.qing.servicessepush.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class PageController {

    @GetMapping("/")
    public String sseClientPage() {
        return "forward:sse-client.html";
    }
}

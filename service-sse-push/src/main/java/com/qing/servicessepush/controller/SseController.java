package com.qing.servicessepush.controller;

import com.qing.internalcommon.util.SsePrefixUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@RestController
@Slf4j
public class SseController {

    private static final Map<String, SseEmitter> emitterMap = new HashMap<>();

    /**
     * 建立连接
     * @param userId 用户id
     * @param identity 身份
     * @return
     */
    @GetMapping(path = "/connect")
    public SseEmitter connect(@RequestParam("userId") Long userId, @RequestParam("identity") String identity) {
        log.info("用户ID：{}，身份：{}", userId, identity);
        SseEmitter sseEmitter = new SseEmitter(0L);
        String sseKey = SsePrefixUtil.generateSseKey(userId, identity);
        emitterMap.put(sseKey, sseEmitter);
        return sseEmitter;
    }

    @GetMapping(path = "/push")
    public String push(@RequestParam Long userId, @RequestParam String msg, @RequestParam("identity") String identity) {
        log.info("接收消息，用户ID：{}，身份：{}，消息内容：{}", userId, identity, msg);
        String sseKey = SsePrefixUtil.generateSseKey(userId, identity);
        try {
            if (emitterMap.containsKey(sseKey)) {
                emitterMap.get(sseKey).send(msg);
            } else {
                return "用户离线中";
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return "给用户："+sseKey+"发送了消息："+msg;
    }

    @GetMapping(path = "/close")
    public String closeConnect(@RequestParam Long userId, @RequestParam String identity) {
        String sseKey = SsePrefixUtil.generateSseKey(userId, identity);
        log.info("关闭连接：{}", sseKey);
        if (emitterMap.containsKey(sseKey)) {
            emitterMap.remove(sseKey);
        }
        return "close success";
    }
}

package com.qing.serviceverificationcode.controller;

import com.qing.internalcommon.dto.ResponseResult;
import com.qing.internalcommon.response.NumberCodeResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@Slf4j
public class NumberCodeController {

    @GetMapping(path = "/numberCode/{size}")
    public ResponseResult<NumberCodeResponse> getCode(@PathVariable("size") Integer size) {
        log.info("receive size: {}", size);
        double random = (Math.random() * 9 + 1) * (Math.pow(10, size - 1));
        int randomInt = (int) random;
        NumberCodeResponse numberCodeResponse = new NumberCodeResponse();
        numberCodeResponse.setNumberCode(randomInt);
        log.info("generate source code: {}", randomInt);
        return ResponseResult.success(numberCodeResponse);
    }
}
